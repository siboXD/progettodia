package it.unibo.dia.musicstore.util;

import java.util.List;

public class ListPage<E> {

	private final List<E> pageItems;
	private final int pageIndex;
	private final int itemsPerPage;
	private final int itemCount;
	
	
	public ListPage(List<E> pageItems, int pageIndex,
			int itemsPerPage, long itemCount) {
		this.pageItems = pageItems;
		this.pageIndex = pageIndex;
		this.itemsPerPage = itemsPerPage;
		this.itemCount = (int) itemCount;
	}

	public ListPage(List<E> allItems, int itemsPerPage,
			int pageIndex) {
		this.itemsPerPage = itemsPerPage;
		this.pageIndex = pageIndex;
		this.itemCount = allItems.size();
		int begin = pageIndex * itemsPerPage,
				end = Math.min(begin + itemsPerPage, itemCount);
		this.pageItems = allItems.subList(begin, end);
	}

	public List<E> getItems() {
		return pageItems;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public int getPageNumber() {
		return pageIndex + 1;
	}

	public int getFirstItemNumber() {
		return pageIndex * itemsPerPage + 1;
	}

	public int getLastItemNumber() {
		return Math.min((pageIndex + 1) * itemsPerPage, itemCount);
	}

	public int getItemsPerPage() {
		return itemsPerPage;
	}

	public int getItemCount() {
		return itemCount;
	}

	public int getPageCount() {
		return (itemCount + itemsPerPage - 1) / itemsPerPage;
	}
	
	public int getLastPage(){
		return (int) Math.ceil(itemCount/(double)itemsPerPage);
	}
}
