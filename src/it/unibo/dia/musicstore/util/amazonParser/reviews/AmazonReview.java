package it.unibo.dia.musicstore.util.amazonParser.reviews;

import java.util.Date;

/**
 * Recensione estratta dal dataset Amazon.
 */
public class AmazonReview {

	private String userId;
	private String userName;
	private String productAsin;
	private int helpfulnessPositive;
	private int helpfulnessTotal;
	private Date time;
	private int reviewScore;
	private String reviewSummary;
	private String reviewText;

	/** ID dell'utente che ha scritto la recensione. */
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/** Nome dell'autore della recensione. */
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Codice ASIN identificativo del prodotto recensito.
	 * 
	 * <p>
	 * Nota: è possibile vedere la pagina di un prodotto su Amazon.com all'URL
	 * <code>http://www.amazon.com/dp/(codice_ASIN)</code>
	 * </p>
	 */
	public String getProductAsin() {
		return productAsin;
	}
	public void setProductAsin(String productAsin) {
		this.productAsin = productAsin;
	}

	/**
	 * Numero di utenti che hanno trovato la recensione utile, o 0 se il dato
	 * non è disponibile.
	 */
	public int getHelpfulnessPositive() {
		return helpfulnessPositive;
	}
	public void setHelpfulnessPositive(int helpfulnessPositive) {
		this.helpfulnessPositive = helpfulnessPositive;
	}

	/**
	 * Numero di utenti che hanno valutato l'utilità della recensione, o 0 se il
	 * dato non è disponibile.
	 */
	public int getHelpfulnessTotal() {
		return helpfulnessTotal;
	}
	public void setHelpfulnessTotal(int helpfulnessTotal) {
		this.helpfulnessTotal = helpfulnessTotal;
	}

	/**
	 * Data e ora in cui è stata scritta la recensione.
	 */
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}

	/** Punteggio assegnato dall'utente al prodotto, compreso tra 1 e 5. */
	public int getReviewScore() {
		return reviewScore;
	}
	public void setReviewScore(int reviewScore) {
		this.reviewScore = reviewScore;
	}

	/** Riga di sommario della recensione. */
	public String getReviewSummary() {
		return reviewSummary;
	}
	public void setReviewSummary(String reviewSummary) {
		this.reviewSummary = reviewSummary;
	}

	/** Testo della recensione. */
	public String getReviewText() {
		return reviewText;
	}
	public void setReviewText(String reviewText) {
		this.reviewText = reviewText;
	}
	
	@Override
	public String toString() {
		return "Review [userId=" + userId + ", userName=" + userName + ", productAsin=" + productAsin
				+ ", helpfulnessPositive=" + helpfulnessPositive + ", helpfulnessTotal=" + helpfulnessTotal + ", time="
				+ time + ", reviewScore=" + reviewScore + ", reviewSummary=" + reviewSummary + ", reviewText="
				+ reviewText + "]";
	}

}
