package it.unibo.dia.musicstore.util.amazonParser.reviews;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import it.unibo.dia.musicstore.model.Order;
import it.unibo.dia.musicstore.model.OrderDetail;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.model.Review;
import it.unibo.dia.musicstore.model.User;
import javafx.util.Pair;

/**
 * Classe per salvare le AmazonReview in formato JSON, mappandole sul database
 * per il progetto DIA
 * 
 * E' abbastanza lento perchè quando deve inserire le ultime righe, le query al
 * database diventano pesanti... (dal 75% in poi è molto lento)
 * 
 * è consigliato inoltre creare degli indici prima di lanciare l'esecuzione,
 * siccome alla fine le scansioni sequanziali delle tabelle costano parecchio
 * 
 * Per far funzionare il codice sottostante, il file di configurazione
 * hibernate.cfg.xml deve essere modificato opportunamente;
 * 
 * anche la classe User deve eessere modificata togliendo temporaneamente il
 * generatore di ID siccome glieli dobbiamo dare manualmente
 * 
 * @author Enrico
 *
 */
public class AmazonReviewToDBSaver {

	/*
	 * Variabile che determina quante iterazioni sono fatte prima di un commit,
	 * ricordarsi di cambiare il valore nell'attributo di hibernate nel file
	 * hibernate.cfg.xml perchè sia uguale a questo
	 */
	private static final int OBJECTS_TO_SAVE_PER_TRANSACTION = 100;

	public static void main(String[] args) {

		// I dataset Amazon si possono trovare all'indirizzo:

		// http://jmcauley.ucsd.edu/data/amazon/links.html

		final String sourceUrl = "file:///C:/Users/Enrico/Downloads/reviews_Digital_Music.json.gz";
		// Stesso dataset ma preso dalla rete
		// final String sourceUrl =
		// "http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/reviews_Digital_Music.json.gz";

		try (Session hibernateSession = new Configuration().configure().buildSessionFactory().openSession()) {

			/*
			 * Chiedo al DB fino a dove sono arrivato a inserire gli orderDetail
			 * (stesso numero delle review)
			 */
			int lastInserted = hibernateSession.createQuery("SELECT MAX(id) FROM OrderDetail").uniqueResult() == null
					? 0 : ((int) hibernateSession.createQuery("SELECT MAX(id) FROM OrderDetail").uniqueResult());
			System.out.println("Erano già presenti " + lastInserted + " records --> da saltare");

			/* Creo l'iteratore del file JSON */
			final Iterator<AmazonReview> amazonReviews = new AmazonReviewsReader(sourceUrl, System.err);

			/* Salto le AmazonReview già processate */
			while (lastInserted-- > 0) {
				amazonReviews.next();
			}

			/*
			 * Mappe per gestire gli oggetti creati in ram ma non ancora salvati
			 * su DB
			 */
			final Map<Integer, User> tmpUserMap = new HashMap<>(OBJECTS_TO_SAVE_PER_TRANSACTION * 2);
			final Map<Integer, Product> tmpProductMap = new HashMap<>(OBJECTS_TO_SAVE_PER_TRANSACTION * 2);
			final Map<Pair<Integer, Date>, Order> tmpOrderMap = new HashMap<>(OBJECTS_TO_SAVE_PER_TRANSACTION * 2);

			/*
			 * Siccome è un processo batch faccio tutto in transazioni corpose,
			 * così perdo meno tempo
			 */
			Transaction t = hibernateSession.beginTransaction();

			AmazonReview source; // il buffer temporaneo

			int count = 0;
			while (amazonReviews.hasNext()) {

				source = amazonReviews.next();
				/*
				 * Prendo l'id in formato stringa dello scrittore della
				 * recensione, e lo trasformo in un intero
				 */
				final int userID = Math.abs(source.getUserId().hashCode());

				/*
				 * Se l'id calcolato è già presente sul DB, suppongo sia
				 * un'altra recensione dello stesso utente, (questo può non
				 * essere vero nel caso in cui il codice hash di due ID diversi
				 * venga a coincidere; in quel caso due recensori diventerebbero
				 * la stessa persona)
				 */
				User user;
				boolean newUser = false;

				if (tmpUserMap.containsKey(userID)) {
					/*
					 * Controllo se lo user attuale è presente tra quelli non
					 * ancora salvati sul DB
					 */
					user = tmpUserMap.get(userID);
				} else {
					/*
					 * Altrimenti lo cerco sul DB e se non o trovo ne creo uno
					 * nuovo
					 */

					/*
					 * Controllo aggiunto a posteriori (il secondo nella if): si
					 * controlla che il nome utente non sia già presente nel DB
					 * siccome il nome utente dovrebbe essere univoco nel
					 * dominio applicativo, mentre nel dataset non lo era; gli
					 * utenti con nome utente uguale sono considerati lo stesso
					 * utente
					 */
					if ((user = hibernateSession.get(User.class, userID)) == null
							|| (user = (User) hibernateSession.createQuery("FROM User WHERE name=:name")
									.setParameter("name", source.getUserName()).uniqueResult()) == null) {
						// Utente non ancora presente
						newUser = true;
						user = new User();
						user.setId(userID);
						user.setName(source.getUserName());
						tmpUserMap.put(userID, user);
					}
				}

				/*
				 * Prendo l'id in formato stringa del prodotto e lo trasformo in
				 * intero
				 */
				final int productID = Math.abs(source.getProductAsin().hashCode());

				/*
				 * Qui vale lo stesso discorso per gli utenti, eccetto che si
				 * potrebbe fare in modo di verificare il productAsin al posto
				 * di andare con gli hascode dello stesso... ma per semplicità
				 * continuo così... sapendo che potrei accorpare due o più
				 * prodotti in uno solo
				 */
				Product product;
				boolean newProduct = false;
				/* Controllo tra i prodotti non ancora persistenti */
				if (tmpProductMap.containsKey(productID)) {
					product = tmpProductMap.get(productID);
				} else {
					/* Altrimenti sul DB e se non trovo ne creo uno nuovo */
					product = hibernateSession.get(Product.class, productID);

					if (product == null) {
						// Prodotto non ancora presente
						newProduct = true;
						product = new Product();
						product.setId(productID);
						product.setCode(source.getProductAsin());
						tmpProductMap.put(productID, product);
					}
				}

				/*
				 * Prendo la data dalla AmazonReview e la trasformo in un
				 * Calendar siccome dopo servirà così
				 */
				final Calendar data = Calendar.getInstance();
				data.setTime(source.getTime());

				/*
				 * Se nello stesso giorno un certo utente ha scritto un'altra
				 * recensione, lo considero come ordine di più di un prodotto e
				 * quindi aggiungo un OrderDetail a un vecchio ordine (che qui
				 * sotto ritrovo grazie alla mappadegli ordini), altrimenti creo
				 * un nuovo ordine
				 */
				Order order;
				boolean newOrder = false;
				Pair<Integer, Date> myOrderIdentifier = new Pair<>(userID, source.getTime());
				if (tmpOrderMap.containsKey(myOrderIdentifier)) {
					// Ordine preesistente da aggiornare (tra quelli fatti
					// dall'ultimo commit finora)
					order = tmpOrderMap.get(myOrderIdentifier);

				} else {
					/*
					 * Se l'ordine esiste sul DB ne prendo il riferimento per
					 * aggiornarlo e non eseguo il corpo dell'if; Altrimenti se
					 * l'ordine non c'è nel DB eseguo il corpo dell'if e ne creo
					 * uno nuovo
					 */
					order = ((Order) hibernateSession.createQuery("FROM Order WHERE user=:user AND date=:date")
							.setParameter("user", user).setParameter("date", data).uniqueResult());

					if (order == null) {
						// Nuovo ordine
						newOrder = true;
						order = new Order();
						order.setDate(data);
						order.setUser(user);
						/*
						 * aggiungo un ordine fatto dallo user attualmente
						 * processato nella data attualmente processata
						 */
						tmpOrderMap.put(myOrderIdentifier, order);
					}
				}

				/* Creo il dettaglio ordine collegato a questa recensione */
				OrderDetail orderDetail = new OrderDetail();
				orderDetail.setOrder(order);
				orderDetail.setProduct(product);
				orderDetail.setQuantity(1); // suppongo che la qualtità di ogni
											// dettaglio sia 1

				/* Creo infine la recensione */
				Review review = new Review();
				review.setDate(data);
				review.setFullText(source.getReviewText());
				review.setHelpfulnessPositive(source.getHelpfulnessPositive());
				review.setHelpfulnessTotal(source.getHelpfulnessTotal());
				review.setScore(source.getReviewScore());
				review.setSummary(source.getReviewSummary());

				/* Ora devo settare i vari riferimenti incrociati */
				review.setOrderDetail(orderDetail);
				orderDetail.setReview(review);

				final List<OrderDetail> oDetails = order.getDetails();
				oDetails.add(orderDetail);
				order.setDetails(oDetails);

				/*
				 * Controllo necessario siccome potrei anche aggiornare, un
				 * vecchio ordine con un nuovo dettaglio solamente... e in quel
				 * caso l'utente non ha un nuovo ordine (semplicemente un suo
				 * ordine è stato modificato)
				 */
				if (newOrder) {
					final List<Order> uOrders = user.getOrders();
					uOrders.add(order);
					user.setOrders(uOrders);
				}

				final Set<OrderDetail> pOrders = product.getOrders();
				pOrders.add(orderDetail);
				product.setOrders(pOrders);

				/*
				 * Queste if servono siccome user order e product potrebbero già
				 * essere presenti sul db e in quel caso le modifiche si rendono
				 * persistenti già da se tramite il commit; fare un "save"
				 * risulterebbe in un errore siccome non devono essere nuove
				 * righe nelle rispettive tabelle
				 */
				if (newUser) {
					hibernateSession.save(user);
				}

				if (newOrder) {
					hibernateSession.save(order);
				}

				if (newProduct) {
					hibernateSession.save(product);
				}

				hibernateSession.save(orderDetail);

				hibernateSession.save(review);

				/*
				 * Ogni tot AmazonReview faccio commit, dopodichè riapro una
				 * nuova transazione e riparto
				 */
				if (++count % OBJECTS_TO_SAVE_PER_TRANSACTION == 0) {
					hibernateSession.flush();
					hibernateSession.clear();
					t.commit();
					t = hibernateSession.beginTransaction();
					count = 0;
					tmpOrderMap.clear();
					tmpProductMap.clear();
					tmpUserMap.clear();
				}

				// fine ciclo while
			}

			/* Faccio commit dell'ultima parte di inserimento */
			t.commit();

			// fine try
		}
		System.exit(0);
		// fine main
	}
}