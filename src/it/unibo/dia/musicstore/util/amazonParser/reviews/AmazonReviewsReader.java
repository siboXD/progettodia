package it.unibo.dia.musicstore.util.amazonParser.reviews;

import java.io.PrintStream;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import it.unibo.dia.musicstore.util.amazonParser.LineJSONReader;

/**
 * Lettore del dataset Amazon delle recensioni.
 * 
 * @author Enrico
 *
 */
public class AmazonReviewsReader extends LineJSONReader<AmazonReview> {

	public AmazonReviewsReader(String source, PrintStream progressBarOutput) {
		super(source, progressBarOutput);
	}

	public AmazonReviewsReader(String source) {
		this(source, null);
	}

	@Override
	protected AmazonReview fillElementFromJSON(final JsonObject json) {
		AmazonReview r = new AmazonReview();
		r.setUserId(json.get("reviewerID").getAsString());
		if (json.has("reviewerName")) {
			r.setUserName(json.get("reviewerName").getAsString());
		}
		r.setProductAsin(json.get("asin").getAsString());
		if (json.has("helpful")) {
			JsonArray helpfulness = json.getAsJsonArray("helpful");
			r.setHelpfulnessPositive(helpfulness.get(0).getAsInt());
			r.setHelpfulnessTotal(helpfulness.get(1).getAsInt());
		}
		Date time = new Date(1000L * json.get("unixReviewTime").getAsLong());
		r.setTime(time);
		r.setReviewScore(json.get("overall").getAsInt());
		r.setReviewSummary(json.get("summary").getAsString());
		r.setReviewText(json.get("reviewText").getAsString());
		return r;
	}

}
