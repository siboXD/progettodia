package it.unibo.dia.musicstore.util.amazonParser.metadata;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import it.unibo.dia.musicstore.util.amazonParser.LineJSONReader;

/**
 * Lettore di metadati Amazon
 * 
 * 
 * @author Enrico
 *
 */
public class AmazonMetadataReader extends LineJSONReader<AmazonMetadata> {

	public AmazonMetadataReader(String source, PrintStream progressBarOutput) {
		super(source, progressBarOutput);
	}

	public AmazonMetadataReader(String source) {
		this(source, null);
	}

	@Override
	protected AmazonMetadata fillElementFromJSON(JsonObject json) {
		AmazonMetadata m = new AmazonMetadata();
		m.setProductId(json.get("asin").getAsString());
		if (json.has("title")) {
			m.setProductName(json.get("title").getAsString());
		}
		if (json.has("price")) {
			m.setPrice(json.get("price").getAsDouble());
		}
		if (json.has("imUrl")) {
			m.setImageURL(json.get("imUrl").getAsString());
		}
		if (json.has("related")) {
			final JsonObject relatedProducts = json.getAsJsonObject("related");
			Set<String> tmp;
			if (relatedProducts.has("also_bought")) {
				tmp = new HashSet<>();
				for (JsonElement jsonElement : relatedProducts.getAsJsonArray("also_bought")) {
					tmp.add(jsonElement.getAsString());
				}
				m.setAlsoBoughtProducts(tmp);
			}
			if (relatedProducts.has("also_viewed")) {
				tmp = new HashSet<>();
				for (JsonElement jsonElement : relatedProducts.getAsJsonArray("also_viewed")) {
					tmp.add(jsonElement.getAsString());
				}
				m.setAlsoViewedProducts(tmp);
			}
			if (relatedProducts.has("bought_together")) {
				tmp = new HashSet<>();
				for (JsonElement jsonElement : relatedProducts.getAsJsonArray("bought_together")) {
					tmp.add(jsonElement.getAsString());
				}
				m.setBoughtTogetherProducts(tmp);
			}
		}
		if (json.has("salesRank")) {
			final Map<String, Integer> salesRankings = new HashMap<>();
			for (Entry<String, JsonElement> entry : json.get("salesRank").getAsJsonObject().entrySet()) {
				salesRankings.put(entry.getKey(), entry.getValue().getAsInt());
			}
			m.setInCategoryRankings(salesRankings);
		}
		if (json.has("brand")) {
			m.setBrand(json.get("brand").getAsString());
		}
		final List<List<String>> categoriesLists = new ArrayList<>();
		for (JsonElement jsonElement : json.getAsJsonArray("categories")) {
			final List<String> categories = new ArrayList<>();
			for (JsonElement jsonElementInternal : jsonElement.getAsJsonArray()) {
				categories.add(jsonElementInternal.getAsString());				
			}
			categoriesLists.add(categories);
		}
		m.setCategories(categoriesLists);
		return m;
	}

}
