package it.unibo.dia.musicstore.util.amazonParser.metadata;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import it.unibo.dia.musicstore.model.Category;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.util.CacheHashMap;
import it.unibo.dia.musicstore.util.amazonParser.reviews.AmazonReviewToDBSaver;
import javafx.util.Pair;

/**
 * Classe per il salvataggio dei dati dei prodotti sul database per il progetto
 * DIA
 * 
 * Da eseguire DOPO aver eseguito {@link AmazonReviewToDBSaver}
 * 
 * NOTA: per eseguirlo deve essere modificato il file hibernate.cfg.xml
 * 
 * @author Enrico
 *
 */
public class AmazonMetadataToDBSaver {

	private static final int OBJECTS_PER_TRANSACTION = 1000;

	public static void main(String[] args) {

		final String sourceUrl = "file:///C:/Users/Enrico/Downloads/meta_Digital_Music.json.gz";
		/*
		 * Stesso dataset ma preso dalla rete final String sourceUrl =
		 * "http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/meta_Digital_Music.json.gz";
		 */

		try (Session hibernateSession = new Configuration().configure().buildSessionFactory().openSession()) {

			Transaction t = hibernateSession.beginTransaction();

			/*
			 * Mappa temporanea per gestire gli oggetti creati ma non acncora
			 * salvati sul DB, e per fare caching delle categorie richieste al
			 * DB
			 */
			final CacheHashMap<String, Category> tmpCategoryMap = new CacheHashMap<>(OBJECTS_PER_TRANSACTION / 2);
			/* Mappa temporanea per i nuovi prodotti, e per fare caching */
			CacheHashMap<Integer, Product> tmpProductMap = new CacheHashMap<>(OBJECTS_PER_TRANSACTION * 2);

			Iterator<AmazonMetadata> metadataReader = new AmazonMetadataReader(sourceUrl, System.err);
			AmazonMetadata source;

			int count = 0;
			while (metadataReader.hasNext()) {
				source = metadataReader.next();

				int productID = Math.abs(source.getProductAsin().hashCode());

				Product product;
				/* Controllo le sovrapposizioni di ID dovute all'hasCode */
				if (tmpProductMap.containsKey(productID)) {
					product = tmpProductMap.get(productID);
				} else {
					if ((product = hibernateSession.get(Product.class, productID)) == null) {
						/* Se non ho ancora questo prodotto lo inserisco */
						product = new Product();
						product.setId(productID);
						product.setCode(source.getProductAsin());

						hibernateSession.save(product);
					}

					/* Questi set li devo fare comuqnue */
					product.setBrand(source.getBrand());
					product.setImageUrl(source.getImageURL());
					product.setName(source.getProductName());
					product.setPrice(source.getPrice());

					/* Faccio caching del prodotto caricato/creato */
					tmpProductMap.put(productID, product);
				}

				/*
				 * Scorro le liste dei percorsi verso le categorie del prodotto
				 * in questione
				 */
				for (final List<String> categoryRoute : source.getCategories()) {
					Category category;
					for (int i = 0, size = categoryRoute.size(); i < size; i++) {
						final String actualCatName = categoryRoute.get(i);
						/*
						 * Scorro il percorso dalla categoria padre fino alla
						 * foglia
						 */
						if (tmpCategoryMap.containsKey(actualCatName)) {
							/*
							 * Se ho trovato una categoria conosciuta nelle
							 * ultime create/cercate, la prendo
							 */
							category = tmpCategoryMap.get(actualCatName);

						} else {
							/* Altrimenti la cerco nel DB */
							if ((category = (Category) hibernateSession.createQuery("FROM Category WHERE name=:name")
									.setParameter("name", actualCatName).uniqueResult()) == null) {
								/*
								 * Se non avevo ancora la categoria (nemmeno nel
								 * DB), la creo
								 */
								category = new Category();
								category.setName(actualCatName);

								/*
								 * Faccio già qui il .save(), tanto in ogni caso
								 * le modifiche successive verranno tutte
								 * salvate al commit
								 */
								hibernateSession.save(category);
							}

							/*
							 * Aggingo la categoria creata/recuperata a quelle
							 * conosciute nella mappa temporanea
							 */
							tmpCategoryMap.put(actualCatName, category);
						}

						if (i > 0) {
							/*
							 * Mi è capitata una NullPointerException su
							 * getParent() della categoria "Office Products",
							 * sospetto quasi che in realtà la semantica della
							 * lista di categorie sia diversa... oppure è un
							 * errore nel JSON; in ogni caso risolvo spostando
							 * il controllo sul padre qua fuori (prima lo facevo
							 * subito dopo la creazione della categoria e basta)
							 * in modo che venga eseguito anche in quel caso
							 * (p.s. ho constatato a fine esecuzione che era
							 * l'unico caso, quindi direi una anomalia del
							 * JSON... in ogni caso in questo modo si evita il
							 * crash)
							 */
							if (category.getParent() == null) {
								final String parentCatName = categoryRoute.get(i - 1);
								if (tmpCategoryMap.containsKey(parentCatName)) {
									/*
									 * ho la categoria padre nella mappa
									 * temporanea
									 */
									category.setParent(tmpCategoryMap.get(parentCatName));
								} else {
									/*
									 * prendo la categoria padre dal DB (esiste
									 * per forza)
									 */
									category.setParent(
											(Category) hibernateSession.createQuery("FROM Category WHERE name=:name")
													.setParameter("name", parentCatName).uniqueResult());
									/*
									 * faccio caching di questa categoria per
									 * risparmiare sulle chiamate a DB nelle
									 * prossime iterazioni
									 */
									tmpCategoryMap.put(parentCatName, category.getParent());
								}
							}
							/*
							 * Tutte le volte che NON sono nella radice della
							 * lista aggiungo una categoria figlia se non esiste
							 * già (già fatto automaticamente questo controllo
							 * dalla add implementata dai Set<E>)
							 */
							category.getParent().getChildren().add(category);

						}

						if (i == size - 1) {
							/*
							 * Se la categoria processata è una foglia la
							 * aggiungo tra le categorie di un prodotto, siccome
							 * le categorie superiori si possono sempre ricavare
							 * dalle figlie
							 */
							if (!product.getCategories().contains(category)) {
								product.getCategories().add(category);
							}
						}

						/* Per ogni categoria setto il prodotto corrente */
						if (!category.getProducts().contains(product)) {
							category.getProducts().add(product);
						}
					}

				}

				count++;
				if (count % OBJECTS_PER_TRANSACTION == 0) {
					hibernateSession.flush();

					t.commit();
					t = hibernateSession.beginTransaction();
					count = 0;

					if ((Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) / (1024 * 1024) < 50) {
						/* Se sono a corto di memoria piallo tutto! (ho meno di 50 MB)*/
						hibernateSession.clear();
						tmpCategoryMap.clear();
						tmpProductMap.clear();
						Runtime.getRuntime().gc();
					} else {
						{
							final Set<Pair<String, Category>> deletedPairs = tmpCategoryMap
									.clearRetaining(OBJECTS_PER_TRANSACTION, true);
							/*
							 * Mi faccio ritornare gli elementi eliminati dalla
							 * mappa temporanea in modo da poter liberare la
							 * sessione hibernate dagli stessi
							 */
							for (final Pair<String, Category> pair : deletedPairs) {
								hibernateSession.evict(pair.getValue());
							}
						}

						{
							final Set<Pair<Integer, Product>> deletedPairs = tmpProductMap
									.clearRetaining(OBJECTS_PER_TRANSACTION, true);

							for (final Pair<Integer, Product> pair : deletedPairs) {
								hibernateSession.evict(pair.getValue());
							}
						}
					}
				}
				// fine primo while
			} /* Commentare fin qui se si vuole eseguiro solo la seconda parte */

			t.commit();
			// fine try
		}

		// fine main
		System.exit(0);
	}

}
