package it.unibo.dia.musicstore.util.amazonParser.metadata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Metadati estratti dal dataset Amazon
 * 
 * @author Enrico
 *
 */
public class AmazonMetadata {

	private String productId;
	private String title;
	private double price;
	private String imageURL;
	private Set<String> also_bought = new HashSet<>();
	private Set<String> also_viewed = new HashSet<>();
	private Set<String> bought_together = new HashSet<>();
	private Map<String, Integer> inCategoryRankings = new HashMap<>();
	private String brand;
	private List<List<String>> categories = new ArrayList<>();

	/**
	 * 
	 * @return il codice ASIN del prodotto
	 */
	public String getProductAsin() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 
	 * @return il nome del prodotto
	 */
	public String getProductName() {
		return title;
	}

	public void setProductName(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return il prezzo del prodotto
	 */
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * 
	 * @return l'url dell'immagine del prodotto
	 */
	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	/**
	 * 
	 * @return l'insieme di prodotti acquistati dopo aver acquistato questo
	 */
	public Set<String> getAlsoBoughtProducts() {
		return also_bought;
	}

	public void setAlsoBoughtProducts(Set<String> also_bought) {
		this.also_bought = also_bought;
	}

	/**
	 * 
	 * @return l'insieme dei prodotti visti dopo questo
	 */
	public Set<String> getAlsoViewedProducts() {
		return also_viewed;
	}

	public void setAlsoViewedProducts(Set<String> also_viewed) {
		this.also_viewed = also_viewed;
	}

	/**
	 * 
	 * @return l'insieme dei prodotti acquistati nello stesso acquisto di questo
	 */
	public Set<String> getBoughtTogetherProducts() {
		return bought_together;
	}

	public void setBoughtTogetherProducts(Set<String> bought_together) {
		this.bought_together = bought_together;
	}

	/**
	 * 
	 * @return l'insieme delle coppie "Categoria" -> "Ranking" per questo
	 *         prodotto
	 */
	public Map<String, Integer> getInCategoryRankings() {
		return inCategoryRankings;
	}

	public void setInCategoryRankings(Map<String, Integer> inCategoryRankings) {
		this.inCategoryRankings = inCategoryRankings;
	}

	/**
	 * 
	 * @return il brand di questo prodotto
	 */
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * 
	 * @return le categorie a cui questo prodotto appartiene nel seguente
	 *         formato: una lista contenente il persorso per arrivare alla
	 *         categoria a cui il prodotto appartiene; quindi ogni lista
	 *         semplice contiene, in ordine, il percorso dalla radice
	 *         dell'albero delle categorie fino ad arrivare alla subcategoria di
	 *         appartenenza; quindi siccome un prodotto può appartenere a più
	 *         categorie ci sono più liste, una per categoria specifica (molte
	 *         condividono il nodo radice ovviamente)
	 */
	public List<List<String>> getCategories() {
		return categories;
	}

	public void setCategories(List<List<String>> categoriesLists) {
		this.categories = categoriesLists;
	}

	@Override
	public String toString() {
		return "AmazonMetadata [productId=" + productId + ", title=" + title + ", price=" + price + ", imageURL="
				+ imageURL + ", also_bought=" + also_bought + ", also_viewed=" + also_viewed + ", bought_together="
				+ bought_together + ", inCategoryRanking=" + inCategoryRankings + ", brand=" + brand + ", categories="
				+ categories + "]";
	}

}
