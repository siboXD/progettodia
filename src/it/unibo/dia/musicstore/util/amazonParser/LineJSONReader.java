/**
 * 
 */
package it.unibo.dia.musicstore.util.amazonParser;

import java.io.BufferedReader;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.zip.GZIPInputStream;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Classe per leggere dai dataset UCSD di Amazon.
 * 
 * File JSON compressi (.gz)
 * 
 * <p>
 * La classe legge i dataset completi oppure i "k-core", con un'elemento per
 * riga in formato JSON.
 * </p>
 * 
 * <p>
 * Si possono leggere gli elementi da un file su disco (URL "file:///...") o
 * anche direttamente via HTTP senza bisogno di scaricare il file su disco (URL
 * "http://...").
 * </p>
 * @author Enrico
 *
 */
public abstract class LineJSONReader<E> implements Iterator<E> {

	static final String PROGBAR_HEADER = "0%                   25%                   50%                   75%                100%";

	static final class PositionInputStream extends FilterInputStream {
		private long pos = 0;

		public PositionInputStream(InputStream in) {
			super(in);
		}

		/**
		 * <p>
		 * Get the stream position.
		 * </p>
		 *
		 * <p>
		 * Eventually, the position will roll over to a negative number. Reading
		 * 1 Tb per second, this would occur after approximately three months.
		 * Applications should account for this possibility in their design.
		 * </p>
		 *
		 * @return the current stream position.
		 */
		public synchronized long getPosition() {
			return pos;
		}

		@Override
		public synchronized int read() throws IOException {
			int b = super.read();
			if (b >= 0)
				pos += 1;
			return b;
		}

		@Override
		public synchronized int read(byte[] b, int off, int len) throws IOException {
			int n = super.read(b, off, len);
			if (n > 0)
				pos += n;
			return n;
		}

		@Override
		public synchronized long skip(long skip) throws IOException {
			long n = super.skip(skip);
			if (n > 0)
				pos += n;
			return n;
		}

		@Override
		public boolean markSupported() {
			return false;
		}
	}

	private final JsonParser parser = new JsonParser();
	private final PositionInputStream posStream;
	private final BufferedReader lineReader;
	private boolean endOfStream = false;
	private E buffer;
	private final PrintStream progressBarOutput;
	private long bytesPerProgbarUnit, nextProgbarUnit;

	/**
	 * Crea un nuovo iteratore di file JSON Amazon. Non stampa alcuna barra di
	 * avanzamento.
	 * @param source URL del file da leggere
	 */
	public LineJSONReader(String source) {
		this(source, null);
	}

	/**
	 * Crea un nuovo iteratore di file JSON Amazon.
	 * @param source URL del file da leggere
	 * @param progressBarOutput stream di output su cui stampare una barra di
	 * avanzamento, di solito <code>System.err</code> o <code>System.out</code>,
	 * se <code>null</code> non viene stampata alcuna barra
	 */
	public LineJSONReader(String source, PrintStream progressBarOutput) {
		try {
			URL url = new URL(source);
			URLConnection conn = url.openConnection();
			posStream = new PositionInputStream(conn.getInputStream());
			lineReader = new BufferedReader(new InputStreamReader(
					new GZIPInputStream(posStream)));
			this.progressBarOutput = progressBarOutput;
			if (progressBarOutput != null) {
				long fileSize = conn.getContentLengthLong();
				progressBarOutput.println("compressed size: " +
						fileSize / 1000000 + " MB");
				progressBarOutput.println(PROGBAR_HEADER);
				bytesPerProgbarUnit = fileSize / PROGBAR_HEADER.length();
				nextProgbarUnit = bytesPerProgbarUnit;
			}
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Carica un elemento in memoria, in modo che possa successivamente essere restituito tramite il metodo <code>next()</code>
	 * 
	 * @return <code>true</code> se un elemento è stato caricato in memoria, <code>false</code> altrimenti
	 * @throws IOException
	 */ 
	private boolean fetchElement() throws IOException {
		if (buffer != null) {
			return true;
		}
		if (endOfStream) {
			return false;
		}
		String line = lineReader.readLine();
		if (line == null) {
			closeStream();
			return false;
		}
		JsonObject json = parser.parse(line).getAsJsonObject();
		
		buffer = fillElementFromJSON(json);
		
		if (progressBarOutput != null) {
			long readBytes = posStream.getPosition();
			while (readBytes >= nextProgbarUnit) {
				progressBarOutput.print("=");
				nextProgbarUnit += bytesPerProgbarUnit;
			}
		}
		return true;
	}
	
	/**
	 * Carica in memoria i dati dell'elemento letto in formato JSON.
	 * 
	 * @param json il {@link JsonObject} contenente i dati letti
	 * @return l'elemento riempito dei dati caricati dal {@link JsonObject}
	 */
	protected abstract E fillElementFromJSON(final JsonObject json);

	private void closeStream() throws IOException {
		lineReader.close();
		if (progressBarOutput != null) {
			progressBarOutput.println();
		}
		endOfStream = true;
	}

	@Override
	public boolean hasNext() {
		try {
			return fetchElement();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public E next() {
		try {
			if (!fetchElement()) {
				throw new NoSuchElementException();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		E next = buffer;
		buffer = null;
		return next;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	public void close() {
		if (!endOfStream) {
			try {
				closeStream();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
