package it.unibo.dia.musicstore.util;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import it.unibo.dia.musicstore.model.Order;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.model.User;

/**
 * Ho creato questa classe per risolvere alcuni problemi derivanti
 * dall'estrazione dei dati dai DataSet Amazon
 * 
 * I problemi risolti sono:
 * - Nomi utenti duplicati
 * - Prezzi dei prodotti a 0 e/o vuoti
 * - Utenti senza e-mail
 * - Utenti senza password 
 * 
 * I problemi non risolti: 
 * - Prodotti senza nome
 * - Prodotti senza brand
 * 
 * @author Enrico
 *
 */
public class DBProblemSolver {

	static final String PROGBAR_HEADER = "0%                   25%                   50%                   75%                100%";
	static final int BATCH_SIZE = 500;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		try (Session hibernateSession = new Configuration().configure().buildSessionFactory().openSession()) {
			/* Conto le righe della tabella User */
			long rows = (long) hibernateSession.createQuery("SELECT COUNT(*) FROM User").uniqueResult();
			long percSubdivision = rows / PROGBAR_HEADER.length();
			System.out.println("Risoluzione problemi utenti:\n" + PROGBAR_HEADER);

			Transaction t = hibernateSession.beginTransaction();

			List<User> tmpUserList = new ArrayList<>();

			boolean progbarTrigger = false;
			for (int i = 0; i < rows; i++) {

				/* Prendo gli utenti */
				if (tmpUserList.isEmpty()) {
					tmpUserList = (List<User>) hibernateSession.createQuery("FROM User ORDER BY id").setFirstResult(i)
							.setMaxResults(BATCH_SIZE).list();
				}

				User currentUser = tmpUserList.remove(0);

				/* GESTIONE NOMI VUOTI */
				if (currentUser.getName() == null || currentUser.getName().trim().isEmpty()) {
					currentUser.setName("Utente" + i);
				}

				/* GESTIONE UTENTI DUPLICATI */
				final List<User> duplicates = hibernateSession.createQuery("FROM User WHERE name=:name")
						.setParameter("name", currentUser.getName()).list();

				if (duplicates.size() > 1) {
					/* Ho trovato un utente duplicato */
					duplicates.remove(currentUser);

					/* Per ogni altro utente */
					for (User u : duplicates) {
						/* Per ogni suo ordine */
						for (Order o : u.getOrders()) {
							/*
							 * Cambio l'utente che lo ha eseguito, nel primo
							 * utente
							 */
							o.setUser(currentUser);
						}
						/* Sposto gli ordini tutti sul primo */
						currentUser.getOrders().addAll(u.getOrders());
						/* Elimino gli ordini dell'utente duplicato */
						u.getOrders().clear();
						/* Elimino l'utente duplicato */
						hibernateSession.delete(u);
					}
					
					tmpUserList.removeAll(duplicates);
					
					if (i % percSubdivision > i % ((rows - duplicates.size()) / PROGBAR_HEADER.length())) {
						/*
						 * Controllo se ho passato la soglia per la progressBar
						 */
						progbarTrigger = true;
					} else {
						/*
						 * Ogni volta che trovo un utente duplicato e lo sistemo
						 * in ogni caso voglio fare un commit
						 */
						t.commit();
						t = hibernateSession.beginTransaction();
					}
					/*
					 * Sistemo il numero massimo di utenti, visto che
					 *  ho eliminato i duplicati e non li scorro più
					 */
					rows = rows - duplicates.size();
					percSubdivision = rows / PROGBAR_HEADER.length();
				}

				String diaSuffix = "@dia.unibo.it".intern();
				String chiocciola = "@".intern();

				/* GESTIONE EMAIL VUOTE */
				if (currentUser.getMailAddress() == null || currentUser.getMailAddress().trim().isEmpty()) {
					if (currentUser.getName().contains(chiocciola)) {
						currentUser.setMailAddress(currentUser.getName());
					} else {
						currentUser.setMailAddress(currentUser.getName() + diaSuffix);
					}
				}

				/* GESTIONE PASSWORD VUOTE */
				if (currentUser.getPassword() == null || currentUser.getPassword().trim().isEmpty()) {
					currentUser.setPassword("password");
				}

				if (i != 0 && (i % percSubdivision == 0 || progbarTrigger)) {
					hibernateSession.flush();
					hibernateSession.clear();
					t.commit();
					tmpUserList.clear();
					t = hibernateSession.beginTransaction();
					System.out.print("=");
					progbarTrigger = false;
				}
			}

			t.commit();
			hibernateSession.clear();
			tmpUserList.clear();

			System.out.println("\n Risoluzione problemi prodotti\n" + PROGBAR_HEADER);
			/* Conto le righe dalla tabella Prodotti */
			rows = (long) hibernateSession.createQuery("SELECT COUNT(*) FROM Product").uniqueResult();
			percSubdivision = rows / PROGBAR_HEADER.length();

			List<Product> tmpProductList = new ArrayList<>();
			
			t = hibernateSession.beginTransaction();

			for (int i = 0; i < rows; i++) {

				/* Prendo i prodotti */
				if (tmpProductList.isEmpty()) {
					tmpProductList = (List<Product>) hibernateSession.createQuery("FROM Product ORDER BY id").setFirstResult(i)
							.setMaxResults(BATCH_SIZE).list();
				}

				
				Product product = tmpProductList.remove(0);

				/* GESTIONE PREZZI AZZERATI */
				if(product.getPrice() == null || product.getPrice() == 0) {
					product.setPrice(1.0);
				}

				if (i % percSubdivision == 0) {
					hibernateSession.flush();
					hibernateSession.clear();
					tmpProductList.clear();
					t.commit();
					t = hibernateSession.beginTransaction();
					System.out.print("=");
				}
			}

			t.commit();
			hibernateSession.clear();

			// fine try
		}

		// fine main
		System.exit(0);
	}
}
