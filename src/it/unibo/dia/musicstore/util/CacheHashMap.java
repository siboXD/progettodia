package it.unibo.dia.musicstore.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javafx.util.Pair;

/**
 * Una mappa che contiene solo gli elementi più acceduti
 * 
 * @author Enrico
 *
 * @param <K>
 *            keys type
 * @param <V>
 *            values type
 */
public class CacheHashMap<K, V> extends HashMap<K, V> {

	private static final long serialVersionUID = 6425583217409393629L;

	private final Map<K, Integer> keyAccessCounterMap;

	public CacheHashMap() {
		this.keyAccessCounterMap = new HashMap<>();
	}

	public CacheHashMap(int initialCapacity) {
		super(initialCapacity);
		this.keyAccessCounterMap = new HashMap<>(initialCapacity);
	}

	public CacheHashMap(Map<? extends K, ? extends V> m) {
		super(m);
		this.keyAccessCounterMap = new HashMap<>(m.size());
		for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
			this.keyAccessCounterMap.put(entry.getKey(), 0);
		}
	}

	public CacheHashMap(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
		this.keyAccessCounterMap = new HashMap<>(initialCapacity, loadFactor);
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key) {
		V value = super.get(key);
		if (value != null) {
			// System.out.println("outMap: "+value+" --> accessMap:
			// "+this.keyAccessCounterMap.get(key));
			incrementAccessCounterFor((K) key);
		}
		return value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V getOrDefault(Object key, V defaultValue) {
		V value = super.getOrDefault(key, defaultValue);
		if (!value.equals(defaultValue)) {
			incrementAccessCounterFor((K) key);
		}
		return value;
	}

	@Override
	public V put(K key, V value) {
		V oldValue = super.put(key, value);
		if (!this.keyAccessCounterMap.containsKey(key)) {
			this.keyAccessCounterMap.put(key, 0);
		}
		return oldValue;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		super.putAll(m);
		for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
			if (!this.keyAccessCounterMap.containsKey(entry.getKey())) {
				this.keyAccessCounterMap.put(entry.getKey(), 0);
			}
		}
	}

	@Override
	public V putIfAbsent(K key, V value) {
		V oldValue = super.putIfAbsent(key, value);
		if (this.keyAccessCounterMap.containsKey(key) && oldValue == null) {
			this.keyAccessCounterMap.put(key, 0);
		}
		return oldValue;
	}

	@Override
	public void clear() {
		super.clear();
		this.keyAccessCounterMap.clear();
	}

	@Override
	public boolean remove(Object key, Object value) {
		boolean removed = super.remove(key, value);
		if (removed) {
			this.keyAccessCounterMap.remove(key);
		}
		return removed;

	}

	@Override
	public V remove(Object key) {
		V value = super.remove(key);
		this.keyAccessCounterMap.remove(key);
		return value;
	}

	/**
	 * Metodo per aggiornare il contatore degli accessi ad una chiave
	 * 
	 * @param key
	 *            la chiave di cui aggiornare il contatore
	 */
	private void incrementAccessCounterFor(K key) {
		Objects.requireNonNull(key);
		this.keyAccessCounterMap.put(key, this.keyAccessCounterMap.get(key) + 1);
	}

	/**
	 * Metodo per mantenere nella mappa solo i mapping più utilizzati; elimina
	 * tutti i mapping poco usati che nella classifica hanno una posizione > di
	 * entriesToRetain; in ogni caso azzera i contatori dei più usati per
	 * ripartire da capo; se richiesto, ritorna il set delle entry cancellate
	 * 
	 * @param entriesToRetain
	 *            l numero di entry da mantenere nella mappa, tra le piu
	 *            accedute (se è 0 o <0 è come eseguire un clear())
	 * @param getDeletedEntrySet
	 *            booleano per richiedere il set delle entry cancellate
	 * @return se getDeletedSet è <code>true</code> ritorna il set delle entry (chiave -> valore) cancellate;
	 *         altrimenti <code>null</code>
	 */
	public Set<Pair<K,V>> clearRetaining(int entriesToRetain, boolean getDeletedEntrySet) {
		if (getDeletedEntrySet) {
			if (entriesToRetain <= 0) {
				Set<Pair<K,V>> deleted = new HashSet<>();
				for (Entry<K,V> entry : super.entrySet()) {
					deleted.add(new Pair<>(entry.getKey(), entry.getValue()));
				}
				this.clear();
				return deleted;
			} else {
				int presentEntries = this.keyAccessCounterMap.entrySet().size();
				if (entriesToRetain < presentEntries) {
					Set<Pair<K,V>> deleted = new HashSet<>();
					if (entriesToRetain < presentEntries / 2) {
						/*
						 * Se la parte della mappa da tenere è molto piccola
						 * rispetto all'intera mappa --> mi copio solo gli
						 * elementi che mi servono cancellando il resto -->
						 * Ordino la mappa in modo decrescente per valori
						 */
						final Map<K, Integer> orderedMap = sortByComparator(this.keyAccessCounterMap,
								new Comparator<Map.Entry<K, Integer>>() {
									public int compare(Map.Entry<K, Integer> o1, Map.Entry<K, Integer> o2) {
										return (o2.getValue()).compareTo(o1.getValue());
									}
								});
						final Iterator<Entry<K, Integer>> it = orderedMap.entrySet().iterator();
						this.keyAccessCounterMap.clear();
						int count = 0;
						while (it.hasNext() && count++ < entriesToRetain) {
							this.keyAccessCounterMap.put(it.next().getKey(), 0);
						}
						while (it.hasNext()) {
							final K keyToRemove = it.next().getKey();
							final Pair<K,V> pairDeleted = new Pair<>(keyToRemove, super.get(keyToRemove));
							deleted.add(pairDeleted);
							super.remove(keyToRemove);
						}
					} else {
						/*
						 * In questo caso vale la pena cancellare i singoli
						 * oggetti dalla mappa siccome sono pochi --> ordino la
						 * mappa in modo crescente per valori (così i primi
						 * valori sono quelli da cancellare)
						 */

						final Map<K, Integer> orderedMap = sortByComparator(this.keyAccessCounterMap,
								new Comparator<Map.Entry<K, Integer>>() {
									public int compare(Map.Entry<K, Integer> o1, Map.Entry<K, Integer> o2) {
										return (o1.getValue()).compareTo(o2.getValue());
									}
								});
						final Iterator<Entry<K, Integer>> it = orderedMap.entrySet().iterator();
						int count = presentEntries;
						while (it.hasNext() && count-- > entriesToRetain) {
							final K keyToRemove = it.next().getKey();
							final Pair<K,V> pairDeleted = new Pair<>(keyToRemove, super.get(keyToRemove));
							deleted.add(pairDeleted);
							this.keyAccessCounterMap.remove(keyToRemove);
							super.remove(keyToRemove);
						}
						restartAccessCounters();
					}
					return deleted;
				} else {
					restartAccessCounters();
					return Collections.emptySet();
				}
			}
		} else {
			/* Non devo ritornare il set degli eliminati */
			clearRetaining(entriesToRetain);
			return null;
		}
	}

	/**
	 * Metodo per mantenere nella mappa solo i mapping più utilizzati; elimina
	 * tutti i mapping poco usati che nella classifica hanno una posizione > di
	 * entriesToRetain; in ogni caso azzera i contatori dei più usati per
	 * ripartire da capo
	 * 
	 * @param entriesToRetain
	 *            il numero di entry da mantenere nella mappa, tra le piu
	 *            accedute (se è 0 o <0 è come eseguire un clear())
	 * @return il numero di entry eliminate dalla mappa
	 */
	public int clearRetaining(int entriesToRetain) {
		if (entriesToRetain <= 0) {
			int size = this.size();
			this.clear();
			return size;
		} else {
			int presentEntries = this.keyAccessCounterMap.entrySet().size();
			if (entriesToRetain < presentEntries) {
				if (entriesToRetain < presentEntries / 2) {
					/*
					 * Se la parte della mappa da tenere è molto piccola
					 * rispetto all'intera mappa --> mi copio solo gli elementi
					 * che mi servono cancellando il resto --> Ordino la mappa
					 * in modo decrescente per valori
					 */
					final Map<K, Integer> orderedMap = sortByComparator(this.keyAccessCounterMap,
							new Comparator<Map.Entry<K, Integer>>() {
								public int compare(Map.Entry<K, Integer> o1, Map.Entry<K, Integer> o2) {
									return (o2.getValue()).compareTo(o1.getValue());
								}
							});
					final Iterator<Entry<K, Integer>> it = orderedMap.entrySet().iterator();
					this.keyAccessCounterMap.clear();
					int count = 0;
					while (it.hasNext() && count++ < entriesToRetain) {
						this.keyAccessCounterMap.put(it.next().getKey(), 0);
					}
					while (it.hasNext()) {
						super.remove(it.next().getKey());
					}
				} else {
					/*
					 * In questo caso vale la pena cancellare i singoli oggetti
					 * dalla mappa siccome sono pochi --> ordino la mappa in
					 * modo crescente per valori (così i primi valori sono
					 * quelli da cancellare)
					 */

					final Map<K, Integer> orderedMap = sortByComparator(this.keyAccessCounterMap,
							new Comparator<Map.Entry<K, Integer>>() {
								public int compare(Map.Entry<K, Integer> o1, Map.Entry<K, Integer> o2) {
									return (o1.getValue()).compareTo(o2.getValue());
								}
							});
					final Iterator<Entry<K, Integer>> it = orderedMap.entrySet().iterator();
					int count = presentEntries;
					while (it.hasNext() && count-- > entriesToRetain) {
						final K keyToRemove = it.next().getKey();
						this.keyAccessCounterMap.remove(keyToRemove);
						super.remove(keyToRemove);
					}
					restartAccessCounters();
				}
			} else {
				restartAccessCounters();
			}
			return presentEntries < entriesToRetain ? 0 : presentEntries - entriesToRetain;
		}
	}

	/**
	 * Metodo per far ripartire i contatori delle chiavi all'interno, NON
	 * elimina i dati
	 */
	private void restartAccessCounters() {
		for (Map.Entry<K, Integer> entry : this.keyAccessCounterMap.entrySet()) {
			entry.setValue(0);
		}
	}

	/**
	 * Metodo per ordinare la mappa degli accessi tramie un comparatore passato
	 * 
	 * @param unsortMap
	 *            la mappa da ordinare
	 * @return la mappa ordinata
	 */
	private Map<K, Integer> sortByComparator(final Map<K, Integer> unsortMap,
			final Comparator<Map.Entry<K, Integer>> comparator) {

		// Convert Map to List
		final List<Map.Entry<K, Integer>> list = new LinkedList<Map.Entry<K, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, comparator);

		// Convert sorted map back to a Map
		final Map<K, Integer> sortedMap = new LinkedHashMap<K, Integer>();
		for (Iterator<Map.Entry<K, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<K, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
