package it.unibo.dia.musicstore.suggest;

import it.unibo.dia.musicstore.model.Product;

public class AssociationRule {

	private Product antecedent, consequent;
	private double confidence;

	public AssociationRule() {}

	public AssociationRule(Product antecedent, Product consequent,
			double confidence) {
		this.antecedent = antecedent;
		this.consequent = consequent;
		this.confidence = confidence;
	}

	public Product getAntecedent() {
		return antecedent;
	}

	public void setAntecedent(Product antecedent) {
		this.antecedent = antecedent;
	}

	public Product getConsequent() {
		return consequent;
	}

	public void setConsequent(Product consequent) {
		this.consequent = consequent;
	}

	public double getConfidence() {
		return confidence;
	}

	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}

}
