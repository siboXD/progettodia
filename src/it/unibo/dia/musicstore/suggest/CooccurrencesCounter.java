package it.unibo.dia.musicstore.suggest;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

/**
 * Contatore di cooccorrenze tra oggetti.
 *
 * @param <E> tipo di oggetti
 */
public class CooccurrencesCounter<E> {

	private final HashMap<E, OccurrencesCounter<E>> counts = new HashMap<>();

	private void incOne(E from, E to) {
		OccurrencesCounter<E> itemCounts = counts.get(from);
		if (itemCounts == null) {
			itemCounts = new OccurrencesCounter<>();
			counts.put(from, itemCounts);
		}
		itemCounts.increment(to);
	}

	/**
	 * Restituisce il numero di cooccorrenze tra i due oggetti.
	 */
	public int getCount(E item1, E item2) {
		OccurrencesCounter<E> itemCounts = counts.get(item1);
		return itemCounts == null ? 0 : itemCounts.getCount(item2);
	}

	/**
	 * Incrementa di 1 il numero di cooccorrenze tra i due oggetti.
	 */
	public void increment(E item1, E item2) {
		incOne(item1, item2);
		incOne(item2, item1);
	}

	/**
	 * Incrementa di 1 il numero di cooccorrenze tra tutte le coppie di oggetti
	 * della collezione data.
	 */
	public void incrementAll(Collection<? extends E> items) {
		if (items.size() < 2) {
			return;
		}
		@SuppressWarnings("unchecked")
		E[] array = (E[]) items.toArray();
		for (int i=0; i<array.length; i++) {
			OccurrencesCounter<E> iCounts = counts.get(array[i]);
			if (iCounts == null) {
				iCounts = new OccurrencesCounter<>();
				counts.put(array[i], iCounts);
			}
			for (int j=0; j<array.length; j++) {
				if (i != j) {
					iCounts.increment(array[j]);
				}
			}
		}
	}

	/**
	 * Restituisce l'insieme di tutti gli oggetti che cooccorrono almeno una
	 * volta con l'oggetto dato.
	 */
	public Set<E> getCooccurrentItems(E item) {
		OccurrencesCounter<E> itemCounts = counts.get(item);
		return itemCounts == null ?
				Collections.<E>emptySet() : itemCounts.getCountedItems();
	}

}
