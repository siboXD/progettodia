package it.unibo.dia.musicstore.suggest;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Contatore di occorrenze di oggetti.
 *
 * @param <E> tipo di oggetti
 */
public class OccurrencesCounter<E> {

	private final HashMap<E, Integer> counts = new HashMap<>();

	/**
	 * Restituisce il numero di occorrenze dell'oggetto dato.
	 */
	public int getCount(E item) {
		Integer count = counts.get(item);
		return count == null ? 0 : count;
	}

	/**
	 * Incrementa di 1 il numero di occorrenze dell'oggetto dato.
	 */
	public void increment(E item) {
		Integer count = counts.get(item);
		counts.put(item, count == null ? 1 : (count+1));
	}

	/**
	 * Incrementa di 1 il numero di occorrenze di tutti gli oggetti nella
	 * collezione data.
	 */
	public void incrementAll(Collection<? extends E> items) {
		for (E item: items) {
			increment(item);
		}
	}

	/**
	 * Restituisce un insieme di tutti gli oggetti con almeno un'occorrenza.
	 */
	public Set<E> getCountedItems() {
		return counts.keySet();
	}

}
