package it.unibo.dia.musicstore.suggest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Product;

/**
 * Oggetto che gestisce il calcolo, la memorizzazione e il reperimento dei
 * suggerimenti d'acquisto basati su regole associative.
 */
public class ProductSuggestionsManager {
	
	private static boolean suggestComputed = false;
	
	public static final String STAT_TRANSACTIONS = "transactions";
	public static final String STAT_PRODUCTS = "products";
	public static final String STAT_SUGGESTIONS = "suggestions";
	public static final String STAT_TIME = "time";

	private static final Logger log =
			Logger.getLogger(ProductSuggestionsManager.class.getName());

	private static final String ATTR_PRODUCTS = "ar_products";
	private static final String ATTR_STATS = "ar_stats";
	private static final String ATTR_ALREADY_COMPUTED = "ar_computed";
	private static final Comparator<AssociationRule> RULES_CONFIDENCE_COMP =
			new Comparator<AssociationRule>() {
		@Override
		public int compare(AssociationRule o1, AssociationRule o2) {
			return (int) Math.signum(o2.getConfidence() - o1.getConfidence());
		}
	};
	
	/** Riferimento allo stato globale della webapp. */
	private ServletContext application;
	/** DAO per l'accesso al database. */
	private GeneralDAO dao;

	private ProductSuggestionsManager(ServletRequest request) {
		application = request.getServletContext();
		application.setAttribute(ATTR_ALREADY_COMPUTED, "false");
		dao = DataAccess.getGeneralDAO(request);
	}

	/**
	 * Reperisce la lista di oggetti "conseguenti" da suggerire per un dato
	 * oggetto "antecedente".
	 */
	public List<Product> getSuggestedProducts(Product product) {
		@SuppressWarnings("unchecked")
		Map<Integer, List<Integer>> data = (Map<Integer, List<Integer>>)
				application.getAttribute(ATTR_PRODUCTS);
		if (data == null) {
			return Collections.emptyList();
		}
		List<Integer> suggestedIds = data.get(product.getId());
		if (suggestedIds == null) {
			return Collections.emptyList();
		}
		List<Product> suggestions = new ArrayList<>();
		for (int id: suggestedIds) {
			suggestions.add(dao.getProduct(id));
		}
		return suggestions;
	}

	/**
	 * Calcola i suggerimenti per tutti i prodotti, basandosi sugli ordini
	 * presenti nel database.
	 * @param minSupport minimo numero di ordini in cui deve apparire un
	 * prodotto per calcolarne i suggerimenti, espresso come frazione rispetto
	 * al numero totale di ordini analizzati
	 * @param rulesPerProduct massimo numero di suggerimenti da estrarre per
	 * ogni prodotto
	 */
	public void computeSuggestions(double minSupport, int maxRulesPerProduct) {
		log.info("started rule mining");
		final long startTime = System.currentTimeMillis();
		// reperisci tutti gli ordini
		final Collection<List<Product>> transactions = dao.getTransactions();
		System.out.println("transaction: "+transactions.size());
		log.info("got " + transactions.size() + " transactions from DB");
		// conta occorrenze e cooccorrenze
		final OccurrencesCounter<Product> occurrences = new OccurrencesCounter<>();
		final CooccurrencesCounter<Product> cooccurrences = new CooccurrencesCounter<>();
		for (List<Product> products: transactions) {
			occurrences.incrementAll(products);
			cooccurrences.incrementAll(products);
		}
		log.info("counted occurrences for " +
				occurrences.getCountedItems().size() + " products");
		// genera regole associative di interesse
		final Map<Product, List<AssociationRule>> rules =
				generateAssociationRules(transactions.size(), occurrences,
				cooccurrences, minSupport, maxRulesPerProduct);
		// estrai suggerimenti dalle regole generate
		final Map<Integer, List<Integer>> suggestions = new HashMap<>();
		int suggestionsCount = 0;
		for (Map.Entry<Product, List<AssociationRule>> entry: rules.entrySet()) {
			List<Integer> suggestedIds = new ArrayList<>();
			for (AssociationRule rule: entry.getValue()) {
				// verifica che l'antecedente della regola sia la chiave corrente
				if (!rule.getAntecedent().equals(entry.getKey())) {
					throw new RuntimeException("regola restituita da "
							+ "generateAssociationRules fuori posto!");
				}
				suggestedIds.add(rule.getConsequent().getId());
			}
			suggestions.put(entry.getKey().getId(), suggestedIds);
			suggestionsCount += suggestedIds.size();
		}
		final long elapsedTime = System.currentTimeMillis() - startTime;
		log.info("extracted " + suggestionsCount + " rules");
		// compila statistiche
		final Map<String, Object> stats = new HashMap<>();
		stats.put(STAT_TRANSACTIONS, transactions.size());
		stats.put(STAT_PRODUCTS, occurrences.getCountedItems().size());
		stats.put(STAT_SUGGESTIONS, suggestionsCount);
		stats.put(STAT_TIME, elapsedTime);
		// salva dati nello stato dell'applicazione
		application.setAttribute(ATTR_PRODUCTS, suggestions);
		application.setAttribute(ATTR_STATS, stats);
		suggestComputed = true;
	}
	
	/**
	 * 
	 * @return Ritorna se i suggerimenti per i prodotti sono già stati calcolati o meno
	 */
	public boolean suggestionsComputed(){
		return suggestComputed;
	}
	
	/**
	 * Restituisce statistiche generate dal calcolo dei suggerimenti.
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getComputationStatistics() {
		return (Map<String, Object>) application.getAttribute(ATTR_STATS);
	}

	public static ProductSuggestionsManager get(ServletRequest request) {
		return new ProductSuggestionsManager(request);
	}

	/**
	 * Riordina le regole associative della lista data per confidenza
	 * decresente.
	 */
	static void sortByConfidence(List<AssociationRule> rules) {
		Collections.sort(rules, RULES_CONFIDENCE_COMP);
	}

	/**
	 * Mantiene solamente i primi N elementi della lista data, rimuovendo quelli
	 * eventualmente in eccesso.
	 */
	static void limitToSize(List<?> list, int size) {
		if (list.size() > size) {
			list.subList(size, list.size()).clear();
		}
	}

	/**
	 * Estrai regole associative dai dati forniti.
	 * @param totalTransactions numero totale di transazioni
	 * @param occurrences occorrenze dei singoli prodotti
	 * @param cooccurrences cooccorrenze tra coppie di prodotti
	 * @param minSupport minimo numero di ordini in cui deve apparire un
	 * prodotto per calcolarne i suggerimenti, espresso come frazione rispetto
	 * al numero totale di ordini analizzati
	 * @param rulesPerProduct massimo numero di suggerimenti da estrarre per
	 * ogni prodotto
	 * @return mappa che associa ad ogni prodotto una lista di regole con quel
	 * prodotto per antecedente, ordinate per confidenza decrescente e limitate
	 * alla quantit� fissata
	 */
	static Map<Product, List<AssociationRule>> generateAssociationRules(
			int totalTransactions,
			OccurrencesCounter<Product> occurrences,
			CooccurrencesCounter<Product> cooccurrences,
			double minSupport, int maxRulesPerProduct) {
		// calcola numero minimo di ordini per prodotto
		int minPurchases = (int) (minSupport * totalTransactions);
		Map<Product, List<AssociationRule>> allRules = new HashMap<>();
		// per ogni possibile antecedente...
		for (Product antecedent: occurrences.getCountedItems()) {
			// verifica che il supporto raggiunga il minimo
			int antecedentOccurrences = occurrences.getCount(antecedent);
			if (antecedentOccurrences < minPurchases) {
				continue;
			}
			// crea lista di regole, calcolando la confidenza di ciascuna
			List<AssociationRule> rules = new ArrayList<>();
			for (Product consequent: cooccurrences
					.getCooccurrentItems(antecedent)) {
				double confidence = (double)
						cooccurrences.getCount(antecedent, consequent) /
						antecedentOccurrences;
				AssociationRule rule =
						new AssociationRule(antecedent, consequent, confidence);
				rules.add(rule);
			}
			// ordina la lista per confidenza decresente
			sortByConfidence(rules);
			// taglia la lista al massimo di regole per prodotto
			limitToSize(rules, maxRulesPerProduct);
			// associa la lista all'antecedente
			allRules.put(antecedent, rules);
		}
		return allRules;
	}

}
