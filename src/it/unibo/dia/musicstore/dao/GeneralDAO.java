package it.unibo.dia.musicstore.dao;

import java.util.Collection;
import java.util.List;

import it.unibo.dia.musicstore.model.Category;
import it.unibo.dia.musicstore.model.Order;
import it.unibo.dia.musicstore.model.OrderDetail;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.model.Review;
import it.unibo.dia.musicstore.model.User;
import it.unibo.dia.musicstore.util.ListPage;

/**
 * Interfaccia che rappresenta il Data Access Object  generico per l'applicazione
 * @author Enrico
 *
 */
public interface GeneralDAO {

	/**
	 * Ritorna un prodotto dato il suo ID
	 * @param id l'<code>id</code> del prodotto da trovare
	 * @return il {@link Product} se trovato, altrimenti <code>null</code>
	 */
	Product getProduct(int id);

	/**
	 * @param id l'<code>id</code> della categoria da trovare
	 * @return La {@link Category} se trovata, altrienti <code>null</code>
	 */
	Category getCategory(int id);

	/**
	 * 
	 * @param id l'<code>id</code> dell'ordine da trovare
	 * @return l'{@link Order} se trovato, altrimenti <code>null</code>
	 */
	Order getOrder(int id);

	/**
	 * 
	 * @param id l'<code>id</code> del dettaglio ordine da trovare
	 * @return l'{@link OrderDetail} se trovato, altrimenti <code>null</code>
	 */
	OrderDetail getOrderDetail(int id);

	/**
	 * 
	 * @param id l'<code>id</code> della recensione da trovare
	 * @return la {@link Review} se trovato, altrimenti <code>null</code>
	 */
	Review getReview(int id);
	
	/**
	 * 
	 * @param id l'<code>id</code> dell'utente da trovare
	 * @return l'{@link User} se trovato, altrimenti <code>null</code>
	 */
	User getUser(int id);

	/**
	 * 
	 * @param name il <code>nome</code> dell'utente da trovare
	 * @return l'{@link User} se trovato, altrimenti <code>null</code>
	 */
	User getUserByName(String name);

	/**
	 * La lista delle categorie che non hanno una categoria padre
	 * @return la {@link List}<{@link Category}> contenente queste categorie, altrimenti una lista vuota
	 */
	List<Category> getTopLevelCategories();

	/**
	 * 
	 * @param keyWord la parola chiave su cui verrà fatta la ricerca
	 * @return la {@link List}<{@link Product}> contenente i prodotti i cui nomi fanno match con la <code>keyWord</code>
	 */
	List<Product> searchProducts(String keyWord);
	
	/**
	 * 
	 * @param keyWord la parola chiave su cui verrà fatta la ricerca
	 * @param itemsPerPage il numero di elementi che si vuole vengano restituiti per pagina
	 * @param page il numero della pagina di cui stiamo cercando i prodotti
	 * @return la {@link ListPage}<{@link Product}> che rappresenta il risultato paginato
	 */
	ListPage<Product> searchProductsPaged(String keyWord,
			int itemsPerPage, int page);

	/**
	 * 
	 * @param order l'{@link Order} da inserire
	 * @return l'<code>id</code> dell'{@link Order} appena inserito
	 */
	int insertOrder(Order order);
	
	/**
	 * inserisce l'utente che ha effettuato la registrazione ne db
	 * @param utente da inserire
	 */
	 int insertUser(User user);

	/**
	 * 
	 * @param review la {@link Review} da inserire
	 * @return l'<code>id</code> della {@link Review} inserita
	 */
	int insertReview(Review review);
	
	/**
	 * 
	 * @param num: numero di elementi da estrarre da inserire
	 * @return gli ultimi "num" prodotti inseriti
	 */
	List<Product> getLastProducts(int num);

	/**
	 * 
	 * @return metodo che ritorna la collezione di liste di prodotti che sono stati oggetto di stesse transazioni
	 */
	Collection<List<Product>> getTransactions();
	
}
