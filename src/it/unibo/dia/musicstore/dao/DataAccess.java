package it.unibo.dia.musicstore.dao;

import javax.servlet.ServletRequest;

public class DataAccess {

	/** Nome dell'attributo da settare col DAO in ogni richiesta. */
	public static final String ATTR_DAO = "dao";

	/**
	 * Restituisce il DAO configurato per la richiesta corrente.
	 * @param request la richiesta corrente
	 * @return DAO utilizzabile per accedere al database
	 */
	public static GeneralDAO getGeneralDAO(ServletRequest request) {
		return (GeneralDAO) request.getAttribute(ATTR_DAO);
	}

}
