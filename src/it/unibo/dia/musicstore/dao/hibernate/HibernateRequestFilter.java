package it.unibo.dia.musicstore.dao.hibernate;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import it.unibo.dia.musicstore.dao.DataAccess;

public class HibernateRequestFilter implements Filter {

	private static final Logger logger =
			Logger.getLogger(HibernateRequestFilter.class.getName());

	private SessionFactory sessionFactory;

	public void init(FilterConfig filterConfig) {
		try {
			// Create SessionFactory from standard hibernate.cfg.xml file
			Configuration config = new Configuration();
			config.configure();
			sessionFactory = config.buildSessionFactory();
			logger.info("initialized");
		} catch (Throwable ex) {
			// Log the exception.
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			HibernateGeneralDAO dao = new HibernateGeneralDAO(session);
			request.setAttribute(DataAccess.ATTR_DAO, dao);
			chain.doFilter(request, response);
			session.getTransaction().commit();
		} catch (ServletException ex) {
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw ex;
		} finally {
			if (session != null && session.isOpen()) {
				session.flush();
				session.clear();
				session.close();
			}
		}

	}

	public void destroy() {
		sessionFactory.close();
		logger.info("closed");
	}

}
