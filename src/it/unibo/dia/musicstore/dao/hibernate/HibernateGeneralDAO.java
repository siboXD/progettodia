package it.unibo.dia.musicstore.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.type.IntegerType;

import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.*;
import it.unibo.dia.musicstore.util.ListPage;



@SuppressWarnings("unchecked")
public class HibernateGeneralDAO implements GeneralDAO {

	/* Si può usare logger per stampare messaggi di log nell'output di Tomcat.
	 * es.: logger.info("Hello!");
	 */
	@SuppressWarnings("unused")
	private static final Logger logger =
			Logger.getLogger(HibernateGeneralDAO.class.getName());

	private static final String HQL_USER_BY_NAME =
			"FROM User WHERE name=:name";
	private static final String HQL_TOP_CATEGORIES =
			"FROM Category WHERE parent IS NULL ORDER BY name";
	private static final String HQL_PRODUCT_BY_NAME_WITH =
			"SELECT DISTINCT p FROM Product AS p JOIN p.categories pc WHERE p.name LIKE :pattern OR lower(pc.name) LIKE lower(:pattern) ORDER BY p.name";
	private static final String HQL_PRODUCT_BY_NAME_WITH_COUNT =
			"SELECT COUNT(*) FROM Product WHERE name LIKE :pattern";
	private static final String HQL_LAST_PRODUCTS =
			"FROM Product ORDER BY oid DESC";
	
	
	private final Session hb;

	public HibernateGeneralDAO(Session session) {
		hb = session;
	}

	@Override
	public Product getProduct(int id) {
		return hb.get(Product.class, id);
	}

	@Override
	public Category getCategory(int id) {
		return hb.get(Category.class, id);
	}

	@Override
	public Order getOrder(int id) {
		return hb.get(Order.class, id);
	}

	@Override
	public OrderDetail getOrderDetail(int id) {
		return hb.get(OrderDetail.class, id);
	}


	@Override
	public Review getReview(int id) {
		return hb.get(Review.class, id);
	}
	
	@Override
	public User getUser(int id) {
		return hb.get(User.class, id);
	}

	@Override
	public List<Category> getTopLevelCategories() {
		return hb.createQuery(HQL_TOP_CATEGORIES).list();
	}

	@Override
	public User getUserByName(String name) {
		return (User) hb.createQuery(HQL_USER_BY_NAME)
				.setString("name", name)
				.uniqueResult();
	}

	@Override
	public List<Product> searchProducts(String query) {
		return hb.createQuery(HQL_PRODUCT_BY_NAME_WITH)
				.setString("pattern", "%" + query + "%")
				.list();
	}

	@Override
	public int insertOrder(Order order) {
		Integer orderId = (Integer) hb.save(order);
		return orderId;
	}

	@Override
	public int insertReview(Review review) {
		Integer reviewId = (Integer) hb.save(review);
		return reviewId;
	}

	@Override
	public ListPage<Product> searchProductsPaged(String query,
			int itemsPerPage, int page) {
		String pattern = "%" + query + "%";
		long count = (long) hb.createQuery(HQL_PRODUCT_BY_NAME_WITH_COUNT)
				.setParameter("pattern", pattern)
				.uniqueResult();
		List<Product> pageItems = hb.createQuery(HQL_PRODUCT_BY_NAME_WITH)
				.setParameter("pattern", pattern)
				.setMaxResults(itemsPerPage)
				.setFirstResult(itemsPerPage * page)
				.list();
		return new ListPage<>(pageItems, page, itemsPerPage, count);
	}

	@Override
	public List<Product> getLastProducts(int num) {
		return (List<Product>) hb.createQuery(HQL_LAST_PRODUCTS)
				.setMaxResults(num)
				.list();
	}

	@Override
	public int insertUser(User user) {
		return (Integer) hb.save(user);
	}
	

	@Override
	public Collection<List<Product>> getTransactions() {
			//da controllare!!!!
		
		 /* Per massimizzare l'efficienza, piuttosto che lasciare che Hibernate
		 *
		 * reperisca tutti gli oggetti InvoiceEntry completi, usiamo una query
		 * "nativa" SQL per ottenere solo gli ID e creiamo noi gli oggetti
		 * Product contenenti solamente gli ID.
		 */
		List<Object[]> entries = hb.createSQLQuery(
				"SELECT order_oid, product_oid FROM musicstore.orderdetail")
				.addScalar("order_oid", IntegerType.INSTANCE)
				.addScalar("product_oid", IntegerType.INSTANCE)
				.list();
		Map<Integer, Product> products = new HashMap<>();
		Map<Integer, List<Product>> transactions = new HashMap<>();
		for (Object[] entry: entries) {
			int productId = (int) entry[1];
			Product product = products.get(productId);
			if (product == null) {
				product = new Product();
				product.setId(productId);
				products.put(productId, product);
			}
			int invoiceId = (int) entry[0];
			List<Product> transaction = transactions.get(invoiceId);
			if (transaction == null) {
				transaction = new ArrayList<>();
				transactions.put(invoiceId, transaction);
			}
			transaction.add(product);
		}
		return transactions.values();
		
	}

}
