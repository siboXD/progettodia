package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Category;

@WebServlet("/category")
public class CategoryServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idgenere = Integer.parseInt(request.getParameter("id"));
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		Category cat = dao.getCategory(idgenere); //estraggo la categoria con id corrispondente
		Set<Category> setCat = cat.getChildren(); 
		//estraggo il set di sottocategoria corrispondenti al genere scelto
		if(setCat.isEmpty()){//quando sono arrivata alla foglia dell'abero estraggo i prodotti
			//richiamo catalogo
			request.setAttribute("id", cat.getId());
			request.getRequestDispatcher("catalogo").forward(request, response);
			return;
		}
		Map<Integer, String> mapCat = new HashMap<>();
		for(Category elem : setCat){
			//per ogni elemento del set salvo in una mappa il suo nome e il suo id
			mapCat.put(elem.getId(), elem.getName());
		}
		int prec = cat.getId(); 
		if(cat.getParent() != null){
			prec = cat.getParent().getId();
		}
		request.setAttribute("prec", prec);		
		request.setAttribute("genere", cat);//passo la categoria che rappresenta il genere
		request.setAttribute("mappaFigli", mapCat ); //passo la mappa dei figli della categoria che rappresenta il genere
		request.getRequestDispatcher("/WEB-INF/view/Category.jsp").forward(request, response);
	}

}

