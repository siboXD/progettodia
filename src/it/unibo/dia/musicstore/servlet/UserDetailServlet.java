package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.model.User;
import it.unibo.dia.musicstore.session.Session;

/**
 * Servlet implementation class UserDetailServlet
 * Gestisce "l'account" dell'utente.Stampa le sue informazioni
 * @author Dalila
 */
@WebServlet("/userDetail")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = Session.getSessionManager(request).getCurrentUser(); //ottiene l'id dello User corrente
		if (user == null) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED); 
			//impossibile che succeda visto che accedo al DettaglioUtente solo dopo aver fatto il login; ma per sicurezza meglio un controllo in più
		}
		request.setAttribute("user", user);
		request.getRequestDispatcher("/WEB-INF/view/UserDetail.jsp").forward(request, response);
	}


}
