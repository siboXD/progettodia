package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.OrderDetail;

/**
 * Inoltra la richiesta alla pagina InsertReview.jps in modo che un user possa lasciare una
 *  review sul prodotto acquistato
 * @author Dalila
 */
@WebServlet("/review")
public class ReviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("conf", null);
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		int idDetOrd = Integer.parseInt(request.getParameter("idDetOrd"));
		OrderDetail oDetail = dao.getOrderDetail(idDetOrd);
		request.setAttribute("idOrder", oDetail.getOrder().getId());
		request.setAttribute("idDetOrd", idDetOrd);
		request.setAttribute("prodottoImg", oDetail.getProduct().getImageUrl());
		request.setAttribute("prodottoID", Integer.parseInt(request.getParameter("prodottoID")));
		request.getRequestDispatcher("/WEB-INF/view/InsertReview.jsp").forward(request, response);
	}

}
