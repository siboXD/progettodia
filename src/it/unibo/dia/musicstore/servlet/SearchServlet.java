package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Category;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.util.ListPage;

/**
 * Servlet implementation class SearchServlet
 * Implemente la ricerca
 * @author Dalila
 */
@WebServlet("/search")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int PRODUCTS_FOR_PAGE = 6;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		String keyWord = request.getParameter("cerca");
		String pageStr = request.getParameter("page");
		int page = 0;
		if(pageStr != null){
			 page = Integer.parseInt(pageStr);
		}
		long time = System.currentTimeMillis();
		ListPage<Product> results = dao.searchProductsPaged(keyWord, PRODUCTS_FOR_PAGE, page);
		time = System.currentTimeMillis() - time;
		
		Map<Integer,Integer> mappaPC = new HashMap<>();
		for(Product prod : results.getItems()){
			Set<Category> setCat = prod.getCategories();
			if (setCat.size() == 1) {
				mappaPC.put(prod.getId(), setCat.iterator().next().getId()); //mappa idProd-idCategoria del prodotto
			}else {
				for(Category cat : setCat){
					if(cat.getChildren().isEmpty()){
						mappaPC.put(prod.getId(), cat.getId()); //mappa idProd-idCategoria del prodotto
					}
				}
			}
			if (mappaPC.isEmpty()) {
				mappaPC.put(prod.getId(), null);
			}
		}
		
		request.setAttribute("mappaPC", mappaPC);
		request.setAttribute("cerca", keyWord);
		request.setAttribute("results", results);
		request.setAttribute("time", time);
		request.getRequestDispatcher("/WEB-INF/view/SearchResults.jsp").forward(request, response);
	}


}
