package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.model.ShoppingCart;
import it.unibo.dia.musicstore.session.Session;
import it.unibo.dia.musicstore.session.SessionManager;

/**
 * Servlet implementation class LogoutServlet
 * Gestisce il logout dell'utente
 * @author Dalila
 * 
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionManager sm = Session.getSessionManager(request);
		sm.logUserOut();
		ShoppingCart cart= sm.getCart();
		cart.clear();
		sm.setCart(cart);
		response.sendRedirect("home");
	}

}
