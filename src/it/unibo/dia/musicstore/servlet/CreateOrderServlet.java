package it.unibo.dia.musicstore.servlet;

import java.io.IOException;

import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Order;
import it.unibo.dia.musicstore.model.OrderDetail;
import it.unibo.dia.musicstore.model.ShoppingCart;
import it.unibo.dia.musicstore.model.User;
import it.unibo.dia.musicstore.session.Session;
import it.unibo.dia.musicstore.session.SessionManager;

/**
 * Questa classe permette di creare un nuovo ordine e il suo relativo Detail.
 *  Aggiorno, poi, lo stato del carrello.
 * @author Dalila
 */
@WebServlet("/createOrder")
public class CreateOrderServlet extends HttpServlet {

	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		SessionManager sm = Session.getSessionManager(request);
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		ShoppingCart cart = sm.getCart(); //ottengo il carrello 
		
		// controllo che l'elemento non sia vuoto
		if (cart.isEmpty()) {
		    throw new IllegalStateException("Errore!!!Carrello vuoto");
		}
		//controllo se l'user è loggato
		User user = sm.getCurrentUser();
		if (user == null) {
		    throw new IllegalStateException("Errore!! User NON loggato");
		}
		//Creo un nuovo ordine
		Order ordine = new Order();
		ordine.setDate(Calendar.getInstance());
		ordine.setUser(user);
		
		//inserisco gli elementi del carrello
		for(ShoppingCart.Entry elemento : cart.getEntries()) {
			OrderDetail entry = new OrderDetail();
			entry.setOrder(ordine);
			entry.setProduct(elemento.getProduct());
			entry.setQuantity(elemento.getQuantity());
			ordine.getDetails().add(entry);
		}
		// inserisco l'ordine nel db
		int orderId = dao.insertOrder(ordine);
		
		// svuoto il carrello e lo risetto 
		cart.clear();
		sm.setCart(cart);
		// rimando alla pagina di dettaglio ordine
		response.sendRedirect(request.getContextPath() + "/orderDetail?id=" + orderId);
	}

}
