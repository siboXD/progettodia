package it.unibo.dia.musicstore.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.ShoppingCart;
import it.unibo.dia.musicstore.session.Session;
import it.unibo.dia.musicstore.session.SessionManager;
/**
 * Classe implementata per effettuare le modifiche sul carrello.
 * Aggiunta, rimozione, svuotameto sono le operazioni possibili.
 * Il carello non viene passato ma viene gestito con una session (set/get Cart() )
 * @author Dalila
 * */


@WebServlet("/cartUpdate")
public class CartUpdateServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		SessionManager session = Session.getSessionManager(request);
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		ShoppingCart cart = session.getCart(); //ottengo il carrello
		
		//controllo quale operazione voglio eseguire
		if ("add".equals(action)) {
			int prodId = Integer.parseInt(request.getParameter("prod"));
			int quantity = Integer.parseInt(request.getParameter("qnt"));
			cart.addProduct(dao.getProduct(prodId), quantity);
		} else if ("rem".equals(action)) {
			int prodId = Integer.parseInt(request.getParameter("prod"));
			cart.removeProduct(dao.getProduct(prodId));
		} else if ("clear".equals(action)) {
			cart.clear();
		} else {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "invalid action: " + action);
			return;
		}//setto il carrello e passo alla servlet che permette di vedere la lista di prod del carrello
		session.setCart(cart);
		response.sendRedirect("cart");
	}

}
