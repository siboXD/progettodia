package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Order;
import it.unibo.dia.musicstore.model.OrderDetail;
import it.unibo.dia.musicstore.model.Product;

/**
 * Servlet implementation class OrderDetailServlet
 * Implementa la gestione di tutti i dettagli dell'ordine passato.
 * @author Dalila
 */
@WebServlet("/orderDetail")
public class OrderDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idOrder = Integer.parseInt(request.getParameter("idOrder"));
		double prezzoTP = 0; //variabile che memorizza il prezzo di un prod relativo alla sua quantità
		double totImporto = 0; //variabile che memorizza l'importo totale dell'ordine => accumulatore
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		Order ord = dao.getOrder(idOrder);
		List<OrderDetail> listOrdDetail = ord.getDetails();
			for(OrderDetail ordDet : listOrdDetail){ 
				//per ogni dettaglio estraggo il prodotto e calcolo il costo tot (prezzo unitario*quantità)
				Product prod = ordDet.getProduct();
				prezzoTP = prod.getPrice() * ordDet.getQuantity();
				totImporto += prezzoTP;
				
			}
		request.setAttribute("totImporto", totImporto);
		request.setAttribute("order", ord);
		request.setAttribute("listOrdDet", listOrdDetail);
		request.getRequestDispatcher("/WEB-INF/view/OrderDetail.jsp").forward(request, response);
	}

}
