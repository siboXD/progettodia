package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.model.Order;
import it.unibo.dia.musicstore.model.User;
import it.unibo.dia.musicstore.session.Session;

/**
 * Servlet implementation class OrderServlet
 * Implementa la gestione di tutti gli ordini relativi all'utente loggato.
 * @author Dalila
 */
@WebServlet("/ordini")
public class OrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User usr = Session.getSessionManager(request).getCurrentUser();
		List<Order> listOrders = usr.getOrders(); //ottengo tutti gli ordini relativi a una persona
		request.setAttribute("listOrders", listOrders);
		request.getRequestDispatcher("/WEB-INF/view/Orders.jsp").forward(request, response);

	
	}
}
