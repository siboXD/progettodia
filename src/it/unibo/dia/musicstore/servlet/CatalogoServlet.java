package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.util.ListPage;
import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Category;
import it.unibo.dia.musicstore.model.Product;

/**
 * Servlet implementation class CategoryDetailServlet
 * Visualizza tutti i prodotti della categoria scelta. 
 * Per visualizzarli usufruisco della ListPage.
 * @author Dalila
 */


@WebServlet("/catalogo")
public class CatalogoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int PRODUCTS_FOR_PAGE = 6;
       
	
	 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		int idCatFiglia = Integer.parseInt(request.getParameter("id"));
		Category catChild = dao.getCategory(idCatFiglia);
		
		//controllo se la categoria figlia ha dei nipoti... in quel caso la rispedisco alla CategoryServlet
		Set<Category> setNipoti = catChild.getChildren();
		if(!setNipoti.isEmpty()){
			request.setAttribute("id", idCatFiglia);
			request.getRequestDispatcher("category").forward(request, response);
		}else{
			
			//ottengo il set di prodotti relativo a quella categoria	
			List<Product> listProd = catChild.getProducts(); 
			//devo visualizzare i prodotti usando la ListPage
			String pageStr = request.getParameter("page");
			int page = 0;
			if(pageStr != null){
				 page = Integer.parseInt(pageStr);
			}
			
			ListPage<Product> results = null;
			
			if (listProd != null && !listProd.isEmpty()) {
				results = new ListPage<Product>(listProd, PRODUCTS_FOR_PAGE, page);
				if(results.getItemCount() == 0){
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "Errore estrazione dei prodotti per pagina");
					return;
				}
			}
			
			request.setAttribute("results", results); //passo la lista dei prodotti della pagina
			request.setAttribute("categoria", catChild); //passo la categoria figlia
			request.getRequestDispatcher("/WEB-INF/view/Catalogo.jsp").forward(request, response);
		}
	 }

}
