package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.model.Category;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.model.ShoppingCart;
import it.unibo.dia.musicstore.session.Session;
import it.unibo.dia.musicstore.session.SessionManager;
import it.unibo.dia.musicstore.util.ListPage;
/**
 * Estrae la lista dei prodotti inseriti nel carrello.
 * Li suddivido per pagine. Precisamente due prodotti per pagina.
 * @author Dalila
 **/

@WebServlet("/cart")

public class CartListServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final int PRODUCTS_FOR_PAGE = 2;
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		//ottengo il set di prodotti del carrello
		SessionManager session = Session.getSessionManager(request);
		ShoppingCart carrello = session.getCart();
		double tot = 0;
		if(!carrello.isEmpty()){
			//devo visualizzare i prodotti usando la ListPage
			String pageStr = request.getParameter("page");
			int page = 0;
			if(pageStr != null){
				 page = Integer.parseInt(pageStr);
			}
			//estreggo i prodotti dal carrello e calcolo la spesa tot
			List<Product> listProd = new ArrayList<>();
			Map<Integer,Integer> mappaPC = new HashMap<>();
			for(ShoppingCart.Entry entry : carrello.getEntries()){
				listProd.add(entry.getProduct());
				tot += entry.getQuantity() * entry.getProduct().getPrice(); 
				
				Product prod = entry.getProduct();
				
				Set<Category> setCat = prod.getCategories();
				if (setCat.size() == 1) {
					mappaPC.put(prod.getId(), setCat.iterator().next().getId()); //mappa idProd-idCategoria del prodotto
				}else {
					for(Category cat : setCat){
						if(cat.getChildren().isEmpty()){
							mappaPC.put(prod.getId(), cat.getId()); //mappa idProd-idCategoria del prodotto
						}
					}
				}
				if (mappaPC.isEmpty()) {
					mappaPC.put(prod.getId(), null);
				}
			}
						
			ListPage<Product> results = new ListPage<Product>(listProd, PRODUCTS_FOR_PAGE, page);
			if(results.getItemCount() == 0){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "Errore estrazione dei prodotti per pagina");
				return;
			}
			request.setAttribute("results", results);
			request.setAttribute("mappaPC", mappaPC);
		}else{
			tot = 0;
			request.setAttribute("results", null);
		}
		
		
		request.setAttribute("totale", tot);
		request.setAttribute("carrello", carrello);
		request.getRequestDispatcher("/WEB-INF/view/CartList.jsp").forward(request, response);
		
	}

}
