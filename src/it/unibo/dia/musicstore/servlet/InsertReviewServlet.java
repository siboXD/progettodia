package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.*;
import it.unibo.dia.musicstore.session.Session;
import it.unibo.dia.musicstore.session.SessionManager;

/**
 * Inserisce la review nel DB
 * @author Dalila
 */
@WebServlet("/insertReview")
public class InsertReviewServlet extends HttpServlet {


		private static final long serialVersionUID = 1L;

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			int idDetOrdine = Integer.parseInt(request.getParameter("idDetOrd"));
			SessionManager sm = Session.getSessionManager(request);
			GeneralDAO dao = DataAccess.getGeneralDAO(request);
			// controllo l' user 
			OrderDetail oDetail = dao.getOrderDetail(idDetOrdine);
			Order ordine = oDetail.getOrder();
			if (!ordine.getUser().equals(sm.getCurrentUser())) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "trying to update review of another user");
			}
			//creo la review
			Review review = new Review();
			review.setSummary(request.getParameter("summary"));
			review.setFullText(request.getParameter("fullText"));
			review.setScore(Integer.parseInt(request.getParameter("score")));
			review.setOrderDetail(oDetail);
			oDetail.setReview(review);
			// update su db
			dao.insertReview(review);
			// reindirizzo a insertReview.jsp in modo da avere l'avvenuta conferma
			request.setAttribute("idDetOrd", idDetOrdine);
			request.setAttribute("prodottoImg", oDetail.getProduct().getImageUrl());
			request.setAttribute("conf", true);
			request.getRequestDispatcher("/WEB-INF/view/InsertReview.jsp").forward(request, response);
		}

	}
