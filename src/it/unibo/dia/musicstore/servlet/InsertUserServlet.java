package it.unibo.dia.musicstore.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.User;
import it.unibo.dia.musicstore.session.Session;
import it.unibo.dia.musicstore.session.SessionManager;

/**
 * Servlet implementation class InsertsDataServlet
 * Implementa la registrazione di un utente nel db
 */
@WebServlet("/insertUser")
public class InsertUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		String usernameInput = request.getParameter("username");
		User user = dao.getUserByName(usernameInput); 
		if(user != null){
			request.setAttribute("conf", "false");
			request.getRequestDispatcher("/WEB-INF/view/Registrazione.jsp").forward(request, response);
			
		}else{
			user = new User();
			user.setName(request.getParameter("username"));
			user.setMailAddress(request.getParameter("mail"));
			user.setPassword(request.getParameter("psw"));
			dao.insertUser(user);
			SessionManager sm = Session.getSessionManager(request);
			sm.logUserIn(user);
			request.setAttribute("conf", true);
			request.getRequestDispatcher("/WEB-INF/view/Registrazione.jsp").forward(request, response);
		}
	}

}
