package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.User;
import it.unibo.dia.musicstore.session.Session;
import it.unibo.dia.musicstore.session.SessionManager;

/**
 * Servlet implementation class LoginServlet
 * @author Dalila
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String returnTo = request.getHeader("Referer");
		String username = request.getParameter("username");
		String password = request.getParameter("psw");
		SessionManager sm = Session.getSessionManager(request);
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		User user = dao.getUserByName(username);
		if (user != null && password.equals(user.getPassword())) {
			sm.logUserIn(user);
		} else {
			sm.setLoginErrorMessage("Username e/o password non validi");
		}
		response.sendRedirect(returnTo);
	}

}
