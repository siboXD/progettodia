package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Category;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.suggest.ProductSuggestionsManager;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final int LIMIT_NUMBER = 3;
	private static final int MAX_SUGGESTED_PRODUCTS = 5;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		
		//estraggo le immagini degli ultimi 3 prodotti inseriti
		List<Product> prodList = dao.getLastProducts(LIMIT_NUMBER);
		if(prodList.isEmpty()){
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Errore estrazione ultimi prodotti");
			return;
		}
		Map<Integer,Integer> mappaPC = new HashMap<>();
		for(Product prod : prodList){
			Set<Category> setCat = prod.getCategories();
			if (setCat.size() == 1) {
				mappaPC.put(prod.getId(), setCat.iterator().next().getId()); //mappa idProd-idCategoria del prodotto
			}else {
				for(Category cat : setCat){
					if(cat.getChildren().isEmpty()){
						mappaPC.put(prod.getId(), cat.getId()); //mappa idProd-idCategoria del prodotto
					}
				}
			}
			if (mappaPC.isEmpty()) {
				mappaPC.put(prod.getId(), null);
			}
		}
		
		//estraggo tutte le categorie per avere i generi (categorie senza padre)
		List<Category> listCat = dao.getTopLevelCategories();
		if(listCat.isEmpty()){
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Errore estrazione categorie");
			return;
		}
		Map<Integer,String> generi = new HashMap<>();
		for(Category cat : listCat){
				generi.put(cat.getId(), cat.getName());//mappa id-nome della categoria
		}
		request.setAttribute("prodList", prodList);
		request.setAttribute("mappaPC", mappaPC );
		request.setAttribute("generi", generi);//passo la mappa delle categorie alla pagina Home.jsp
		request.getRequestDispatcher("WEB-INF/view/Home.jsp").forward(request, response);
		
		
		final ProductSuggestionsManager m = ProductSuggestionsManager.get(request);
		if (!m.suggestionsComputed()) {
			/* se ancora non sono stati calcolati i suggerimenti lancio il calcolo */
			m.computeSuggestions(0.00001, MAX_SUGGESTED_PRODUCTS);
			System.out.println("cumputed suggestions");
		}
	}

}
