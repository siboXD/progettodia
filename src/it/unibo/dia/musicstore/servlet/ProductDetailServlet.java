package it.unibo.dia.musicstore.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Category;
import it.unibo.dia.musicstore.model.OrderDetail;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.model.User;
import it.unibo.dia.musicstore.suggest.ProductSuggestionsManager;
import it.unibo.dia.musicstore.session.Session;

/**
 * Servlet implementation class ProductDetailServlet
 * Estrae i dettagli di un prodotto e i conseguenti di se stesso.
 * Attraverso quest'ultimi è possibile creare dei suggerimenti impersonali.
 * @author Dalila
 */
@WebServlet("/productDetail")
public class ProductDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//lettura parametri
		int idprod = Integer.parseInt(request.getParameter("id"));
		int idcat= 0;
		if(request.getParameter("categoria") != null && request.getParameter("categoria") != ""){
			idcat = Integer.parseInt(request.getParameter("categoria"));
		}
		GeneralDAO dao = DataAccess.getGeneralDAO(request);
		Product prodotto = dao.getProduct(idprod); //estraggo il prodotto con id corrispondente
		Category cat = dao.getCategory(idcat);
		
						//suggest's elements	

		//estraggo le categorie relative
		Set<Category> setCat = prodotto.getCategories();
		setCat.remove(cat); //rimuovo la categoria del prod analizzato
		
		//calcolo lo score medio delle review del prodotto
		int scoresSum = 0;
		int totReview = 0;
		for (OrderDetail ordDet: prodotto.getOrders()) {
			if (ordDet.getReview() != null) {
				totReview++;
				scoresSum += ordDet.getReview().getScore();
			}
		}
		if(totReview != 0){
			double averageScore = (int) scoresSum / totReview;
			request.setAttribute("rating", averageScore);
		}else{
			request.setAttribute("rating", "");
		}
				 
		//ottengo l'utente corrente per il controllo del login
		User user = Session.getSessionManager(request).getCurrentUser();
		
		
		ProductSuggestionsManager suggester = ProductSuggestionsManager.get(request);
		List<Product> suggestions = suggester.getSuggestedProducts(prodotto);
		
		List<String> images = new ArrayList<>();
		for (Product suggest : suggestions) {
			images.add(suggest.getImageUrl());
		}
		
		//passaggio dei parametri
		request.setAttribute("suggestions",suggestions);
		request.setAttribute("suggestionsImages", images);
		request.setAttribute("categoria", cat);
		request.setAttribute("setCat", setCat);
		request.setAttribute("prodotto", prodotto); 
		request.setAttribute("user", user);
		request.getRequestDispatcher("/WEB-INF/view/ProductDetail.jsp").forward(request, response);
	}


}