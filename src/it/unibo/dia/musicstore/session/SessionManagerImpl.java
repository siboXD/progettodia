package it.unibo.dia.musicstore.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import it.unibo.dia.musicstore.dao.DataAccess;
import it.unibo.dia.musicstore.dao.GeneralDAO;
import it.unibo.dia.musicstore.model.Product;
import it.unibo.dia.musicstore.model.ShoppingCart;
import it.unibo.dia.musicstore.model.User;

public class SessionManagerImpl implements SessionManager {

	static final String ATTR_USER_ID = "sm_userId";
	static final String ATTR_CART_ITEM_ID = "sm_cartItems";
	static final String ATTR_CART_COUNTS = "sm_cartQuantities";
	static final String ATTR_LOGIN_ERROR = "sm_loginError";

	final HttpSession session;
	final GeneralDAO dao;

	SessionManagerImpl(HttpServletRequest request) {
		session = request.getSession();
		dao = DataAccess.getGeneralDAO(request);
	}

	@Override
	public User getCurrentUser() {
		Integer id = (Integer) session.getAttribute(ATTR_USER_ID);
		return id != null ? dao.getUser(id) : null;
	}

	@Override
	public void logUserIn(User user) {
		session.setAttribute(ATTR_USER_ID, user.getId());
	}

	@Override
	public void logUserOut() {
		session.removeAttribute(ATTR_USER_ID);
	}

	@Override
	public ShoppingCart getCart() {
		ShoppingCart cart = new ShoppingCart();
		int[] ids = (int[]) session.getAttribute(ATTR_CART_ITEM_ID);
		if (ids != null) {
			int[] counts = (int[]) session.getAttribute(ATTR_CART_COUNTS);
			for (int i=0; i<ids.length; i++) {
				Product product = dao.getProduct(ids[i]);
				cart.addProduct(product, counts[i]);
			}
		}
		return cart;
	}

	@Override
	public void setCart(ShoppingCart cart) {
		if (cart.isEmpty()) {
			session.removeAttribute(ATTR_CART_ITEM_ID);
			session.removeAttribute(ATTR_CART_COUNTS);
		} else {
			int[] ids = new int[cart.size()];
			int[] counts = new int[cart.size()];
			for (int i=0; i<ids.length; i++) {
				ShoppingCart.Entry entry = cart.getEntries().get(i);
				ids[i] = entry.getProduct().getId();
				counts[i] = entry.getQuantity();
			}
			session.setAttribute(ATTR_CART_ITEM_ID, ids);
			session.setAttribute(ATTR_CART_COUNTS, counts);
		}
	}

	@Override
	public int getCartSize() {
		int[] ids = (int[]) session.getAttribute(ATTR_CART_ITEM_ID);
		return ids == null ? 0 : ids.length;
	}

	@Override
	public String getLoginErrorMessage() {
		return (String) session.getAttribute(ATTR_LOGIN_ERROR);
	}

	@Override
	public void setLoginErrorMessage(String message) {
		session.setAttribute(ATTR_LOGIN_ERROR, message);
	}

	@Override
	public void clearLoginErrorMessage() {
		session.removeAttribute(ATTR_LOGIN_ERROR);
	}

}
