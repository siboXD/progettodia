package it.unibo.dia.musicstore.session;

import it.unibo.dia.musicstore.model.ShoppingCart;
import it.unibo.dia.musicstore.model.User;

public interface SessionManager {

	public User getCurrentUser();

	public void logUserIn(User user);

	public void logUserOut();

	public ShoppingCart getCart();

	public void setCart(ShoppingCart cart);

	public int getCartSize();

	public String getLoginErrorMessage();

	public void setLoginErrorMessage(String message);

	public void clearLoginErrorMessage();

}
