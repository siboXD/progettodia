package it.unibo.dia.musicstore.session;

import javax.servlet.http.HttpServletRequest;

public final class Session {

	private Session() {}

	public static SessionManager getSessionManager(HttpServletRequest request) {
		return new SessionManagerImpl(request);
	}

}
