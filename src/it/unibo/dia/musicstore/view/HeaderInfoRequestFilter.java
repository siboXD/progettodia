package it.unibo.dia.musicstore.view;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import it.unibo.dia.musicstore.session.Session;
import it.unibo.dia.musicstore.session.SessionManager;

public class HeaderInfoRequestFilter implements Filter {

	public void init(FilterConfig fConfig) {}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		SessionManager session =
				Session.getSessionManager((HttpServletRequest) request);
		request.setAttribute("user", session.getCurrentUser());
		request.setAttribute("cartSize", session.getCartSize());
		request.setAttribute("loginError", session.getLoginErrorMessage());
		chain.doFilter(request, response);
		if (((HttpServletRequest) request).getMethod().equalsIgnoreCase("GET")) {
			session.clearLoginErrorMessage();
		}
	}

	public void destroy() {}

}
