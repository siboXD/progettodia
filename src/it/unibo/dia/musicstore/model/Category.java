package it.unibo.dia.musicstore.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "category")
/**
 * Classe che rappresenta le Categorie di prodotti.
 * 
 * @author Enrico
 *
 */
public class Category {

	private int id;
	private String name;
	private Category parent;
	private Set<Category> children = new HashSet<>();
	private List<Product> products = new ArrayList<>();

	public Category() {
	}

	public Category(int id, String name, Category parent) {
		this.id = id;
		this.name = name;
		this.parent = parent;
	}

	@Id
	@Column(name = "oid")
	@GenericGenerator(name = "inc", strategy = "increment")
	@GeneratedValue(generator = "inc")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// si può non annotare perchè name è anche il nome dell'attributo sul DB
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(optional = true)
	@JoinColumn(name = "category_oid")
	public Category getParent() {
		return parent;
	}

	public void setParent(Category parent) {
		this.parent = parent;
	}

	@OneToMany(mappedBy = "parent")
	public Set<Category> getChildren() {
		return children;
	}

	public void setChildren(Set<Category> children) {
		this.children = children;
	}

	@ManyToMany(mappedBy = "categories")
	@OrderBy("name")
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Category other = (Category) obj;
		return this.id == other.id;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name
				+ (parent != null ? ", parent=[id:" + parent.getId() + ", name:" + parent.getName() + "]" : "") + "]";
	}

}
