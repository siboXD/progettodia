package it.unibo.dia.musicstore.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "order")
/**
 * Rappresenta un ordine fatto da un utente in una certa data.
 * 
 * @author Enrico
 *
 */
public class Order {

	private int id = -1;
	private User user;
	private Calendar date;
	private List<OrderDetail> details = new ArrayList<>();

	public Order() {
	}

	@Id
	@Column(name = "oid")
	@GenericGenerator(name = "inc", strategy = "increment")
	@GeneratedValue(generator = "inc")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "user_oid")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "order")
	public List<OrderDetail> getDetails() {
		return details;
	}

	public void setDetails(List<OrderDetail> details) {
		this.details = details;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Order other = (Order) obj;
		return this.id == other.id;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", user=" + user + ", date=" + date.getTime().toString() + "]";
	}

	

}
