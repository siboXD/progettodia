package it.unibo.dia.musicstore.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user")
/**
 * Rappresenta un utente del dominio applicativo.
 * 
 * @author Enrico
 *
 */
public class User {

	private int id = -1;
	private String name;
	private String password;
	private String mail;
	private List<Order> orders = new ArrayList<Order>();

	public User() {
	}

	@Id
	@Column(name = "oid")
	@GenericGenerator(name = "inc", strategy = "increment")
	@GeneratedValue(generator = "inc")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "username")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String pass) {
		password = pass;
	}

	@Column(name = "email")
	public String getMailAddress() {
		return mail;
	}

	public void setMailAddress(String mail) {
		this.mail = mail;
	}

	@OneToMany(mappedBy = "user")
	@OrderBy("date DESC")
	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public int hashCode() {
		return getId();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		final User other = (User) obj;
		return other.getId() == getId();
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", mail=" + mail + "]";
	}

}
