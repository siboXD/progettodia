package it.unibo.dia.musicstore.model;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShoppingCart extends AbstractCollection<ShoppingCart.Entry> {

    private final List<Entry> entries;

    public ShoppingCart() {
        entries = new ArrayList<Entry>();
    }

    @Override
    public int size() {
    	return entries.size();
    }

    @Override
    public Iterator<Entry> iterator() {
    	return entries.iterator();
    }

    public void addProduct(Product product, int quantity) {
        // if the product is already in the cart, sum the quantities
        for (Entry entry: entries) {
            if (entry.getProduct().equals(product)) {
                entry.setQuantity(entry.getQuantity() + quantity);
                return;
            }
        }
        entries.add(new Entry(product, quantity));
    }

    public boolean removeProduct(Product product) {
        Iterator<Entry> it = entries.iterator();
        while (it.hasNext()) {
            if (it.next().getProduct().equals(product)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    public void clear() {
        entries.clear();
    }

    public List<Entry> getEntries() {
        return new ArrayList<Entry>(entries);
    }

    public boolean isEmpty() {
        return entries.isEmpty();
    }

    public static class Entry {
        private Product product;
        private int quantity;
        Entry(Product product, int quantity) {
            this.product = product;
            this.quantity = quantity;
        }
        public Product getProduct() {
            return product;
        }
        public void setProduct(Product product) {
        	this.product = product;
        }
        public int getQuantity() {
            return quantity;
        }
        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
    }

}
