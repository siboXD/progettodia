package it.unibo.dia.musicstore.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "review")
/**
 * Rappresenta una recensione lasciata da un utente riguardante un prodotto.
 * 
 * @author Enrico
 *
 */
public class Review {

	private int id = -1;
	private int score;
	private String summary;
	private String fullText;
	private int helpfulnessTotal;
	private int helpfulnessPositive;
	private Calendar date;
	private OrderDetail orderDetail;

	public Review() {
	}

	@Id
	@Column(name = "oid")
	@GenericGenerator(name = "inc", strategy = "increment")
	@GeneratedValue(generator = "inc")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "score")
	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "reviewsummary")
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Column(name = "reviewfulltext")
	public String getFullText() {
		return fullText;
	}

	public void setFullText(String text) {
		this.fullText = text;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public Integer getHelpfulnessTotal() {
		return helpfulnessTotal;
	}

	public void setHelpfulnessTotal(Integer helpfulnessTotal) {
		this.helpfulnessTotal = helpfulnessTotal;
	}

	public Integer getHelpfulnessPositive() {
		return helpfulnessPositive;
	}

	public void setHelpfulnessPositive(Integer helpfulnessPositive) {
		this.helpfulnessPositive = helpfulnessPositive;
	}

	@OneToOne
	@JoinColumn(name = "orderdetail_oid")
	public OrderDetail getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(OrderDetail orderDetail) {
		this.orderDetail = orderDetail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Review other = (Review) obj;
		return other.id == this.id;
	}

	@Override
	public String toString() {
		return "Review [id=" + id + ", score=" + score + ", summary=" + summary + ", fullText=" + fullText
				+ ", helpfulnessTotal=" + helpfulnessTotal + ", helpfulnessPositive=" + helpfulnessPositive + ", date="
				+ date + ", orderDetail=[id=" + orderDetail.getId() + "]]";
	}
	
	
}
