<%-- Orderpage
 - gestisce: 
 		visualizzazione degli ordini relativi a una persona
 		
@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" type="text/css" href="style.css">

<%-- Nav! --%>
	  <nav>
	<div id="listCat"><br><a href="home">Home</a>
	<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
	<a href="ordini">Ordini</a></div>
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
<%--stampo gli ordini--%>
<div class="generi">
		<label id="lbTitolo">I miei ordini</label><br><br>
		<c:choose>
			<c:when test="${!empty listOrders}">
				<table id="tblGenere">	
					<tr>
						<c:forEach items="${listOrders}" var="ordine" varStatus="num">
							<c:choose>
								<c:when test="${num.index != 0}"> <%--saltando la prima --%>
									<c:if test="${num.index % 5 == 0}">
										</tr><tr>
									</c:if>
										<td class="orderIcon">
													<div class="Elenco">
													<p>Id: ${ordine.getId()}</p><br>
													<p>Data:
														 <fmt:formatDate value="${ordine.getDate().time}" type="date" dateStyle="long" /></p><br>
													<a href="orderDetail?idOrder=${ordine.getId()}">Visualizza dettagli ordine</a>
												</div>
										 </td> 
									
								</c:when>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
			</c:when>
			<c:otherwise>
				<br/><br/><label id="lbSottotitolo" class="border">Non hai ancora effettuato nessun ordine</label><br/><br/>
				<br><br><br><a href="home">Torna alla home</a><br>
			</c:otherwise>
	  	</c:choose>
		
</div>