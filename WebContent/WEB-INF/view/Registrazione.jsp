<%-- RegistrationPage
  - gestisce: 
 		l'inserimento dei dati dell'utente affinchè possa essere registrato nel database.
 		
@author: Dalila
 --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
	<div id="listCat"><br><a href="home">Home</a>
	<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
	<a href="registrazione">Registrazione</a></div>
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
<div class="generi">
		<label id="lbTitolo">Registrati!</label><br><br><br><br>
		
		<c:choose>
			<c:when test="${!empty conf}">
				<c:if test="${conf == false}"> 
						<label id="lbSottotitolo" class="border">Errore nella registrazione!!!<br><br>Cambia username per favore!<br><br>Riprovare!</label><br><br><br>
						<div id="conf"><img src="images/Error.jpg" alt="Errore "><br><br><br>
						<div class="reg"><a href="registrazione">Riprova</a></div></div> 	
				</c:if>
				<c:if test="${conf == true}"> 
							<label id="lbSottotitolo" class="border">Registrazione effettuata</label><br><br><br><br>
						 	<div id="conf"><img src="images/Okay.jpg"><br><br><br>
						 	<div class="reg"><a href="home">Torna alla home</a></div></div> 
			 	</c:if>	
		 	</c:when>
		  <c:otherwise>
		 		 <label id="lbSottotitolo" class="border">Inserisci i tuoi dati:</label><br>
	             <form id="frmIns" action="insertUser" method="post">
					<label for="username">Username:</label>
					<input type="text" name="username" placeholder="your_username"  required><br>
					<label for="psw">Password:</label>
					<input type="password" name="psw" placeholder="your_password" required><br>
					<label for="username">E-mail:</label>
					<input type="e-mail" name="mail" placeholder="your_mail"  required><br>
					<div id="button"><input id="send" type="submit" value=""><br></div>
				</form> 
	      </c:otherwise>	
	   </c:choose>
</div>
