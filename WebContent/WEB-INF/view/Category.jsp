<%-- Categorypage
 - gestisce: 
 		scelta delle sottocategorie del genere musicale scelto => children di una categoria
 		
@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
	<div id="listCat"><br><a href="home">Home</a>
	<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
	<a href="category?id=${prec}">Categoria</a></div>
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
<%--stampo le categorie figlie --%>
<div class="generi">
		<label id="lbTitolo">${genere.getName()}</label><br><br>
		<label id="lbSottotitolo">Scegli la tua categoria:</label><br>
	<table id="tblGenere">	
		<tr>
			<c:forEach items="${mappaFigli}" var="child" varStatus="num"> 
				<c:if test="${num.index != 0}"> 
					<c:if test="${num.index % 6 == 0}">
						</tr><tr>
					</c:if>
				</c:if>
				<td><a href="catalogo?id=${child.getKey()}">${child.getValue()}</a></td>
			</c:forEach>
		 </tr>
		</table>
</div>