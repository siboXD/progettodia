<%-- CatalogoPage
 - gestisce: 
 		prodotti relativi alla sottocategoria scelta

@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
	<div id="listCatalogo"><br><a href="home">Home</a>
	<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
	<a href="category?id=${categoria.getParent().getParent().getId()}">Categoria</a>
	<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
	<a href="catalogo?id=${categoria.getParent().getId()}">Sottocategoria</a>
	</div>
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
<%--stampo le il catalogo prodotti della categoria passata --%>
<div class="generi">
<label id="lbTitolo">${categoria.getName()}</label><br><br>
<label id="lbSottotitolo">I nostri prodotti:</label><br>
	<c:choose>
	<%-- getItems() restituisce una Lista di elementi  --%>
	
		<c:when test="${!empty results && !empty results.getItems()}">
		    <table id="tblGenere">
		    <tr>
		          <c:forEach items="${results.getItems()}" var="prod" varStatus="num">
			           		<%--per ogni prodotto visualizzo la sua immagine, il codice, il prezzo e 
			           		il link di visualizzazione dettagli che rimanda alla pagina ProductDetail --%>
			           		<c:choose>
			           			<c:when test="${num.index == 0}">
			           					<td>
			           						<a href="productDetail?categoria=${categoria.getId()}&id=${prod.getId()}">
			           							<img src="${prod.getImageUrl()}" alt="immagine del prodotto con codice ${prod.getId()}">
			           						</a><br>
			           						<span class="name">${prod.getId()}</span><br>
			           						<span class="tot"> <fmt:formatNumber value=" ${prod.getPrice()}" type="currency" currencySymbol="&euro;" /></span><br>
			           						<br><a href="productDetail?categoria=${categoria.getId()}&id=${prod.getId()}">Visualizza dettagli</a>
			           	    			</td>
			           			</c:when>
			           			<c:otherwise>
			           				<c:if test="${num.index % 3 == 0}">
			           					</tr><tr>
			           				</c:if>
			           					<td>
			           						<a href="productDetail?categoria=${categoria.getId()}&id=${prod.getId()}">
			           							<img src="${prod.getImageUrl()}" alt="immagine del prodotto con codice ${prod.getId()}">
			           						</a><br>
			           						<span class="name">${prod.getId()}</span><br>
			           						<span class="tot"> <fmt:formatNumber value=" ${prod.getPrice()}" type="currency" currencySymbol="&euro;" /></span><br>
			           						<br><a href="productDetail?categoria=${categoria.getId()}&id=${prod.getId()}">Visualizza dettagli</a>
			           	    			</td>
			           			</c:otherwise>
			           		</c:choose>
			        </c:forEach>			        
		        </tr>
		    </table>
		    <%--scorrimento pagine --%>
		    <c:choose>
		    	<c:when test="${results.getPageIndex() == 0 && results.getPageIndex() != results.getLastPage()}">
	    		 	<%--prima pagina e non unica, solo freccia avanti --%>
	    		 	<div class="inline"><br>
			    		 <p id="paginaNext"><a href="catalogo?id=${categoria.getId()}&page=${results.getPageNumber()}"><img src="images/arrowDX.jpg" alt="Pagina avanti" /></a></p>
		    		</div>
		    	</c:when>
		    	<c:when test="${results.getPageIndex() != 0 && results.getPageIndex() == results.getLastPage()}">
		    		<%--ultima pagina e non unica, solo freccia indietro --%>
		    		<div class="inline"><br>
					     <p id="paginaPrec"><a href="catalogo?id=${categoria.getId()}&page=${results.getPageIndex()-1}"><img src="images/arrowSX.jpg" alt="Pagina indietro" /></a></p>
				    </div>
		    	</c:when>
		    	<c:when test="${results.getPageIndex() == 0 && results.getPageIndex() == results.getLastPage()}">
		    		<%--ultima pagina e unica, no frecce--%>
		    	</c:when>
		    	<c:otherwise>
		    	 <%--pagine in mezzo pagina, entrambe le freccie --%>
				    
					    <p id="paginaPrec"><a href="catalogo?id=${categoria.getId()}&page=${results.getPageIndex()-1}"><img src="images/arrowSX.jpg" alt="Pagina indietro" /></a></p>
					    <p id="paginaNext"><a href="catalogo?id=${categoria.getId()}&page=${results.getPageNumber()}"><img src="images/arrowDX.jpg" alt="Pagina avanti" /></a></p>
				    
			    </c:otherwise>
		    </c:choose>
		</c:when>
		<c:otherwise>
			   <br><br><label id="lbSottotitolo">Non ci sono risultati corrispondenti</label><br/><br/>
			   <br><br><br><a href="home">Torna alla home</a><br>
		</c:otherwise>
	</c:choose>
	 
</div>
