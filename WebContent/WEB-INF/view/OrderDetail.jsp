<%-- OrderDetailpage
 - gestisce: 
 		visualizzazione dei dettagli dell'ordine passato in input
 		
@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" type="text/css" href="style.css">

<%-- Nav! --%>
	<nav>
	<div id="listCatalogo"><br><a href="home">Home</a>
	<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
	<a href="ordini">Ordini</a>
	<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
	<a href="orderDetail?idOrder=${order.getId() }">Dettaglio Ordine</a></div>
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
<%--stampa del dettaglio ordine--%>
<div class="generi">
		<label id="lbTitolo">Codice ordine: ${order.getId()}</label><br><br><br>
		<label id="lbSottotitolo">Data ordine:<fmt:formatDate value="${order.getDate().time}" type="date" dateStyle="long" /></label><br><br>
		
				<table id="tblGenere">	
					
					<c:forEach items="${listOrdDet}" var="detOrdine">
						<tr>
							<td class="Elenco">Id: ${detOrdine.getProduct().getId()}</td>
							<td class="Elenco">Prezzo unitario:<fmt:formatNumber value=" ${detOrdine.getProduct().getPrice()}" 
													type="currency" currencySymbol="&euro;" /> </td>
							<td class="Elenco">Quantità: ${detOrdine.getQuantity()}</td>
							<td class="Elenco">Costo: <fmt:formatNumber value=" ${detOrdine.getQuantity() * detOrdine.getProduct().getPrice()}" 
													type="currency" currencySymbol="&euro;" /></td>
							<c:choose>
								<c:when test="${!empty detOrdine.getReview()}">
									<td class="Elenco">Review:<br> ${detOrdine.getReview().getSummary()}</td>
								</c:when>
								<c:otherwise>
										<td><a href="review?prodottoID=${detOrdine.getProduct().getId()}&idDetOrd=${detOrdine.getId()}">Inserisci Review</a></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
				
				</table><br>
				<hr>
					<div class="tot">Importo Totale dell'ordine:<fmt:formatNumber value=" ${totImporto}" type="currency" currencySymbol="&euro;" /></div>
				<hr>
				
</div>