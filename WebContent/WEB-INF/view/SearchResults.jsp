<%-- SearchPage
 - gestisce: 
 		la stampa del risultato della ricerca

@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
		<div id="listCat"><br><a href="home">Home</a>
		<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
		<a href="#">Ricerca</a></div>
		
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
 
<%--stampo il risultato della ricerca --%>
<div class="generi">
<label id="lbTitolo">Ecco i risultati relativi a ${cerca}:</label><br><br>
<label id="lbSottotitolo">Ricerca completata in ${time} ms.</label><br>
	<c:choose>
	<%-- getItems() restituisce una Lista di elementi  --%>
		<c:when test="${!empty results.getItems()}">
		    <table id="tblGenere">
		    <tr>
		          <c:forEach items="${results.getItems()}" var="prod" varStatus="num">
			           		<%--per ogni prodotto visualizzo la sua immagine, il codice, il prezzo e 
			           		il link di visualizzazione dettagli che rimanda alla pagina ProductDetail --%>
			           		<c:choose>
			           			<c:when test="${num.index == 0}">
			           					<td>
			           					<a href="productDetail?categoria=${mappaPC.get(prod.getId())}&id=${prod.getId()}">
			           						<img src="${prod.getImageUrl()}" alt="immagine del prodotto con codice ${prod.getId()}">
			           					</a>
			           					<br>
			           						<span class="name">${prod.id}</span><br>
			           						<span class="tot"> <fmt:formatNumber value=" ${prod.getPrice()}" type="currency" currencySymbol="&euro;" /></span><br>
			           						<br><a href="productDetail?categoria=${mappaPC.get(prod.getId())}&id=${prod.getId()}">Visualizza dettagli</a>
			           	    			</td>
			           			</c:when>
			           			<c:otherwise>
			           				<c:if test="${num.index % 3 == 0}">
			           					</tr><tr>
			           				</c:if>
			           					<td>
			           						<a href="productDetail?categoria=${mappaPC.get(prod.getId())}&id=${prod.getId()}">
			           							<img src="${prod.getImageUrl()}" alt="immagine del prodotto con codice ${prod.getId()}">
			           						</a>
			           						<br>
			           						<span class="name">${prod.id}</span><br>
			           						<span class="tot"> <fmt:formatNumber value=" ${prod.getPrice()}" type="currency" currencySymbol="&euro;" /></span><br>
			           						<br><a class="border" href="productDetail?categoria=${mappaPC.get(prod.getId())}&id=${prod.getId()}">Visualizza dettagli</a>
			           	    			</td>
			           			</c:otherwise>
			           		</c:choose>
			        </c:forEach>			        
		        </tr>
		    </table>
		    <%--scorrimento pagine --%>
	    	<c:if test="${results.getPageIndex()+1 != results.getLastPage()}">
    		 	<%--prima pagina e non unica, solo freccia avanti --%>
    		 	<div class="inline">
		    		<p id="paginaNext"><a href="search?cerca=${cerca}&page=${results.getPageNumber()}"><img src="images/arrowDX.jpg" alt="Pagina avanti" /></a></p>
	    		</div>
	    	</c:if>
	    	<c:if test="${results.getPageIndex() != 0}">
	    		<%--ultima pagina e non unica, solo freccia indietro --%>
	    		<div class="inline">
				    <p id="paginaPrec"><a href="search?cerca=${cerca}&page=${results.getPageIndex()-1}"><img src="images/arrowSX.jpg" alt="Pagina indietro" /></a></p>
			    </div>
	    	</c:if>
	    	
		</c:when>
		<c:otherwise>
			   <br><br><label class="border" id="lbSottotitolo">Non ci sono risultati corrispondenti</label><br/><br/>
			   <br><br><br><a href="home">Torna alla home</a><br>
		</c:otherwise>
	</c:choose>
	 
</div>

