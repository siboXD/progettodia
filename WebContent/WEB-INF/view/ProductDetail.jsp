<%-- ProductDetailpage
 - gestisce: 
 		il dettaglio del prodotto richiesto
 		stampa suggestion se l'utente non è loggato e recommendation se è loggato

@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
			<div id="listCat">
			<br>
				<a href="home">Home</a>
				<c:if test="${not empty categoria}">
					<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
					<a href="catalogo?id=${categoria.getId()}">Categoria</a>
				</c:if>
			</div>
				<form class="search" action="search" method="get">
						<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
						<input id="btnsearch" type="submit" value="">
				</form>
	 </nav><br>
	 <hr>
 </header>
 <%--stampa dettagli prodotto --%>
 <div class="generi">
		<label id="lbTitolo">
					<c:if test="${!empty prodotto.getName()}">
    					<span class="name">${prodotto.getName()}</span><br>
					</c:if></label><br><br>
		
		<table id="tblGenere">
		    <tr>
		         <td><img src="${prodotto.getImageUrl()}" alt="immagine del prodotto con codice ${prod.getId()}"></td>
			     <td>
			     	<span class="bold">Codice:</span><span class="name"> ${prodotto.getId()}</span><br><br><br>
			     	
			     	<c:if test="${not empty categoria }">
			     		<span class="bold">Category:</span> ${categoria.getName()}<br><br>
			     	</c:if>
			     	<c:if test="${!empty rating}">
			     		<span class="bold">Score:</span> ${rating}<br><br>
			     	</c:if>
			     	<c:if test="${!empty prodotto.getBrand()}">
    					<span class="bold">Brand:</span> ${prodotto.getBrand()}<br><br>
					</c:if>
					<c:if test="${!empty prodotto.getCode()}">
  						<span class="bold">Code:</span> ${prodotto.getCode()}<br><br>
  					</c:if>
			        <span class="tot"> <fmt:formatNumber value=" ${prodotto.getPrice()}" type="currency" currencySymbol="&euro;" /></span><br>
			     </td>
			     <td>
			     	<form id="add" action="cartUpdate" method="post">
							    <input type="hidden" name="action" value="add" />
							    <input type="hidden" name="prod" value="${prodotto.getId()}" />
							    <input type="number" name="qnt" value="1" min="1" max="100" placeholder="quantity" required/><br><br><br>
							    <input id="addCart" type="submit" value="" />
					</form>
					
			     </td>
			</tr>
		</table>
		<br><br><br>
		<c:if test="${!empty setCat}">
			<label class="lbSottotitolo border">Sicuramente ti piaceranno anche queste categorie!</label>
    			<c:forEach items="${setCat}" var="elem" varStatus="st">
           			<c:if test="${st.index < 10}">
           				<c:if test="${st.index != 0 }"> &#126 </c:if>
            			<a class="border" href="category?id=${elem.getId()}">${elem.getName()}</a>
            		</c:if>
        		</c:forEach>
		</c:if>
		<br><br>
		<c:if test="${not empty suggestions}">
		    <label class="lbSottotitolo border"><b>Potresti acquistare:</b></label><br>
		    <c:forEach items="${suggestions}" var="suggested" varStatus="num">
		    	<c:choose>
	   				<c:when test="${num.index < 10}">
	   					<span> </span>
		            	<a class="border" href="productDetail?categoria=${categoria.getId()}&id=${suggested.id}">
							<img class="smallIcon" alt="immagine prodotto suggerito" src="${suggestionsImages.get(num.index)}">
						</a>
		            </c:when>
			 	 </c:choose>
		     
		    </c:forEach>
		</c:if>
</div>