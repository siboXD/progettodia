<%-- UserDetailpage
 - gestisce: 
 		I dati relativi all' "account" dell'utente

@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
		<div id="listCat"><br><a href="home">Home</a>
		<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
		<a href="userDetail">Account</a></div>
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
 
<%--stampo i dati dell'utente --%>
<div class="generi">
		<label id="lbTitolo">Benvenuto/a nel tuo Account</label><br><br>
		<label id="lbSottotitolo">I tuoi dati</label><br><br>
		<table class="tblDettaglio">	
			<tr>
				<td class="det border">Username:</td>
				<td class="border">${user.getName()}</td>
		 	</tr>
		 	<tr>
				<td class="det border">E-mail:</td>
				<td class="border">${user.getMailAddress()}</td>
		 	</tr>
		 	<tr>
				<td class="det border">Numero di ordini effettuati:</td>
				<td class="border">${user.getOrders().size()}</td>
		 	</tr>
	 	 </table>
	 	<table id="tblLink">
	 			<tr>
	 				<td><a class="border" href="ordini">I tuoi ordini</a></td>
	 			</tr>
			 	<tr>
		 			<td><a class="border" href="cart">Il tuo carrello</a></td>
		 		</tr>
	 	</table>
		
</div>