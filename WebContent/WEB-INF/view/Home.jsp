<%-- Homepage
 - gestisce: 
 		scelta dei generi musicali => categoria
 		ultimi inseriti => dettaglio prodotto 

@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
	<div id="listHome"><br><a href="home">Home</a></div>
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
 
<%--stampo le categorie padri --%>
<div class="generi">
		<label id="lbTitolo">Scegli il tuo genere:</label><br>
		<table id="tblGenere">	
		<tr>
			<c:forEach items="${generi}" var="cat" varStatus="num"> <%--ciclo tutte le categorie padri --%>
				
				<c:if test="${num.index != 0}"> <%--saltando la prima --%>
					<c:if test="${num.index % 6 == 0}">
					<%--per tutte le altre controllo se sono divisibili per 6;
					 il tal caso mando la riga a capo e ne apro un'altra
					 Creo così due elementi per colonna e sei colonne ==> 12 categorie --%>
						</tr><tr>
					</c:if>
				</c:if>
				<td><a href="category?id=${cat.getKey()}">${cat.getValue()}</a></td>
			</c:forEach>
		 </tr>
		</table>
</div>

<%--stampo le foto degli ultimi prodotti inseriti
SISTEMA LINK A PRODUCTDETAIL --%>
<div class="generi"><label id="lbTitolo">News!</label><br>
		<table id="tblUscite">
		<tr>
		 <c:forEach items="${prodList}" var="prod" varStatus="n"> <%--ciclo i last prod--%>
						<td>
							 <a href="productDetail?categoria=${mappaPC.get(prod.getId())}&id=${prod.getId()}">
				
									<img src="${prod.getImageUrl()}" alt="immagine del prodotto con codice ${prod.getId()}">
							</a>
						</td>		
			</c:forEach>
		</tr>
		</table>
</div>
