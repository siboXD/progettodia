<%-- CartListPage
 - gestisce: 
 		la stampa dei prodotti presenti nel carrello
@author: Dalila
 --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
		<div id="listCat"><br><a href="home">Home</a>
		<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
		<a href="#">Carrello</a></div>
		
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav>
	 <hr>
 </header>
 
<%--stampo iprodotti--%>
<div class="generi">
<label id="lbTitolo">Il tuo carrello!</label><br><br>
<label id="lbTitolo">Spesa totale: <span class="prezzo"> <fmt:formatNumber value=" ${totale}" type="currency" currencySymbol="&euro;" /></span>

</label><br><br>
  <c:choose>
	<c:when test="${!empty results.getItems()}">
    	<table id="tblGenere">       
        	<c:forEach items="${results.getItems()}" var="prod" varStatus="num">
	  			 <c:if test="${num.index < 2}"> <%--visualizzo il prod--%>
			         <tr>
			         
				        <td><a href="productDetail?categoria=${mappaPC.get(prod.getId())}&id=${prod.getId()}"><img src="${prod.getImageUrl()}" alt="immagine del prodotto" /></a></td>
				                <td>
				                	<c:if test="${!empty prod.getName()}">
			    						<span class="name">${prod.getName()}</span><br><br><br>
									</c:if>
									<span class="bold">Codice: </span><span class="name">${prod.getId()}</span><br><br>
						     		<c:forEach items="${carrello.getEntries()}" var="entry">
        								<c:if test="${prod.getId() == entry.getProduct().getId()}">
        										<span class="bold">Quantità:</span> ${entry.getQuantity()} pezzi<br>
        								</c:if>
        							</c:forEach><br><br>
									<c:if test="${!empty prod.getPrice()}">
						     			<br><span class="bold">Prezzo unitario:</span><fmt:formatNumber value=" ${prod.getPrice()}" type="currency" currencySymbol="&euro;" /><br><br><br>
									</c:if>			     	
						     	</td>
						     	<td> 
						     		
						     		<c:forEach items="${carrello.getEntries()}" var="entry">
	        								<c:if test="${prod.getId() == entry.getProduct().getId()}">
		        								 	<span class="tot"><fmt:formatNumber value="${prod.getPrice() * entry.getQuantity()}" type="currency" currencySymbol="&euro;" /></span>
								     				<br><br><br>
	        								</c:if>
        							</c:forEach>
						     		<form id="add" action="cartUpdate" method="post">
				                		<input type="hidden" name="action" value="rem">
				                		<input type="hidden" name="prod" value="${prod.getId()}">
				                		<input id="remCart" type="submit" value="">
				               	</form>
				             </td>
			             </tr>
			      </c:if>
			</c:forEach>			        
	</table>
	<%--scorrimento pagine --%>
    	<c:if test="${results.getPageIndex()+1 != results.getLastPage()}">
   		 	<%--prima pagina e non unica, solo freccia avanti --%>
   		 	<div class="inline">
	    		 <p id="paginaNext"><a href="cart?page=${results.getPageNumber()}"><img src="images/arrowDX.jpg" alt="Pagina avanti" /></a></p>
    		</div>
    	</c:if>
    	<c:if test="${results.getPageIndex() != 0}">
    		<%--ultima pagina e non unica, solo freccia indietro --%>
    		<div class="inline">
			     <p id="paginaPrec"><a href="cart?page=${results.getPageIndex()-1}"><img src="images/arrowSX.jpg" alt="Pagina indietro" /></a></p>
		    </div>
    	</c:if>
	  </c:when>
	  <c:otherwise>
				    <label id="lbSottotitolo">Non ci sono elementi nel carrello</label><br><br><br><br><br><br>
		  				<a href="home">Torna alla Home</a><br><br><br><br><br>
		 </c:otherwise>
	</c:choose>	
		 <%--controllo se l'utente è loggato, se sì, gli mostro il pulsante 
		    "Prosegui con l'acquisto" altrimenti solo svuota e fai login --%>
	        <c:if test="${!empty user && !empty carrello.getEntries()}">
		        <br><a href="createOrder"><img src="images/ProcAcquisto.jpg" alt="procedi con l'acquisto"></a>
		    </c:if>
		    <c:if test="${empty user && !empty carrello.getEntries()}">
		    	<br>
		       	<label id="cartLoginError">Per proseguire con l'acquisto devi fare il login!</label>
		       	<br><br>
		    </c:if>
		    <c:if test="${!empty carrello.getEntries()}">
			    <form action="cartUpdate" method="post">
			        <input type="hidden" name="action" value="clear" />
			        <input class="clear" type="submit" value="" />
			    </form>
		    </c:if>
</div>
