<%-- InsertReviewPage
  - gestisce: 
 		l'inserimento dei dati della Review
 		
@author: Dalila
 --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- Nav! --%>
	  <nav>
	<div id="listCatalogo"><br><a href="home">Home</a>
	<img src="images/arrowDX.jpg" alt="Immagine della freccia" />
	<a href="orderDetail?idOrder=${idOrder}">Dettaglio Ordine</a></div>
		<form class="search" action="search" method="get">
				<input id="txtsearch" type="text" name="cerca" placeholder="Search" required>     
				<input id="btnsearch" type="submit" value="">
		</form>
	 </nav><br>
	 <hr>
 </header>
<div class="generi">
		<label id="lbTitolo">Inserisci una Review sul prodotto !</label><br><br><br><br>
		<img class="smallIcon" alt="immagine prodotto recensito" src="${prodottoImg}"><br><br><br><br>
		<c:choose>
			<c:when test="${conf == true}"> 
					<label id="lbSottotitolo">Review inviata.<br>Grazie mille!</label><br><br><br><br>
				 	<div id="conf"><img src="images/Okay.jpg"><br><br><br>
				 		<div class="reg border"><a href="home">Torna alla home</a></div>
				 	</div>
	 		</c:when>
		  <c:otherwise>
		 		 <label id="lbSottotitolo">Inserisci la Review:</label><br>
	             <form id="frmIns" action="insertReview?idDetOrd=${idDetOrd}" method="post">
					<label class="bold" for="fullText">FullText:</label>
					<textarea cols="50%" rows="5" name="fullText" placeholder="Full text" required></textarea><br>
					<label class="bold" for="summary">Summary:</label>
					<input type="text" size="50%" name="summary" placeholder="Text's summary.One row. " required><br>
					<label class="bold" for="score">Score:</label>
					<input type="number" name="score" placeholder="your_vote" value="4"  min="1" max="5" required><br>
					<div id="button"><input id="send" type="submit" value=""><br></div>
				</form> 
	      </c:otherwise>	
	   </c:choose>
</div>
