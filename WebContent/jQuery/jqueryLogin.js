/**
 * 
 jQuery per la gestione del login
@author: Dalila
*/
$(document).ready(function(){
	 $("form[name=loginForm]").hide();
	 	
	 $("#login").click(function(){
		$("form[name=loginForm]").slideToggle();
	  });
	 
	 $(window).scroll(function(){
		 if (window.pageYOffset > 0) {
			 $("form[name=loginForm]").hide();
		 }
	 });

});